"""name th for prototype

Revision ID: c69b3b77e564
Revises: f676be6e9641
Create Date: 2016-08-06 12:30:28.805369

"""

# revision identifiers, used by Alembic.
revision = 'c69b3b77e564'
down_revision = 'f676be6e9641'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('prototype', sa.Column('name_th', sa.String(length=150), nullable=True))
    op.create_unique_constraint(None, 'prototype', ['name_th'])


def downgrade():
    op.drop_constraint(None, 'prototype', type_='unique')
    op.drop_column('prototype', 'name_th')
