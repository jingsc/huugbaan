"""add more type of product properties

Revision ID: 21dded62f7ca
Revises: 2b0827cb9075
Create Date: 2016-10-29 17:28:22.816905

"""

# revision identifiers, used by Alembic.
revision = '21dded62f7ca'
down_revision = '2b0827cb9075'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.add_column('prototype', sa.Column('check_properties', postgresql.JSONB(), nullable=True))
    op.add_column('prototype', sa.Column('choose_properties', postgresql.JSONB(), nullable=True))


def downgrade():
    op.drop_column('prototype', 'choose_properties')
    op.drop_column('prototype', 'check_properties')
