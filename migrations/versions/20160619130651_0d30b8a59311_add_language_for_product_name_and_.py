"""add language for product name and scription

Revision ID: 0d30b8a59311
Revises: e02c242fbce0
Create Date: 2016-06-19 13:06:51.546164

"""

# revision identifiers, used by Alembic.
revision = '0d30b8a59311'
down_revision = 'e02c242fbce0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('product', sa.Column('description_en', sa.Text(), nullable=True))
    op.add_column('product', sa.Column('description_th', sa.Text(), nullable=True))
    op.add_column('product', sa.Column('name_en', sa.String(length=250), nullable=True))
    op.add_column('product', sa.Column('name_th', sa.String(length=250), nullable=True))
    op.drop_constraint(u'product_name_key', 'product', type_='unique')
    op.create_unique_constraint(u'product_name_en_key', 'product', ['name_en'])
    op.create_unique_constraint(u'product_name_th_key', 'product', ['name_th'])
    op.drop_column('product', 'description')
    op.drop_column('product', 'name')


def downgrade():
    op.add_column('product', sa.Column('name', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
    op.add_column('product', sa.Column('description', sa.TEXT(), autoincrement=False, nullable=True))
    op.create_unique_constraint(u'product_name_key', 'product', ['name'])
    op.drop_constraint(u'product_name_en_key', 'product', type_='unique')
    op.drop_constraint(u'product_name_th_key', 'product', type_='unique')
    op.drop_column('product', 'name_th')
    op.drop_column('product', 'name_en')
    op.drop_column('product', 'description_th')
    op.drop_column('product', 'description_en')
