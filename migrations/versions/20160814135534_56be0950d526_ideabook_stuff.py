"""ideabook stuff

Revision ID: 56be0950d526
Revises: c2b9b386fdc4
Create Date: 2016-08-14 13:55:34.430856

"""

# revision identifiers, used by Alembic.
revision = '56be0950d526'
down_revision = 'c2b9b386fdc4'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('ideabook_photo',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('assoc_type', sa.String(), nullable=False),
    sa.Column('photo_id', sa.Integer(), nullable=True),
    sa.Column('notes', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('ideabook',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )


def downgrade():
    op.drop_table('ideabook')
    op.drop_table('ideabook_photo')
