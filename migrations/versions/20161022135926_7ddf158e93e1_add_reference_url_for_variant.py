"""add reference url for variant

Revision ID: 7ddf158e93e1
Revises: eadbb4f998f7
Create Date: 2016-10-22 13:59:26.502208

"""

# revision identifiers, used by Alembic.
revision = '7ddf158e93e1'
down_revision = 'eadbb4f998f7'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('variant', sa.Column('reference_url', sa.String(), nullable=True))


def downgrade():
    op.drop_column('variant', 'reference_url')
