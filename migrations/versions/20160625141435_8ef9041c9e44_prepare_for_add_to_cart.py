"""prepare for add to cart

Revision ID: 8ef9041c9e44
Revises: 474fbf14376b
Create Date: 2016-06-25 14:14:35.555889

"""

# revision identifiers, used by Alembic.
revision = '8ef9041c9e44'
down_revision = '474fbf14376b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('address',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('firstname', sa.Unicode(length=200), nullable=True),
    sa.Column('lastname', sa.Unicode(length=200), nullable=True),
    sa.Column('address1', sa.Text(), nullable=True),
    sa.Column('address2', sa.Text(), nullable=True),
    sa.Column('city', sa.String(length=50), nullable=True),
    sa.Column('zipcode', sa.String(length=10), nullable=True),
    sa.Column('phone', sa.String(length=20), nullable=True),
    sa.Column('alternative_phone', sa.String(length=20), nullable=True),
    sa.Column('state_name', sa.String(length=100), nullable=True),
    sa.Column('company', sa.Unicode(length=100), nullable=True),
    sa.Column('email', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('firstname'),
    sa.UniqueConstraint('lastname')
    )
    op.create_table('ord',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('number', sa.String(length=32), nullable=True),
    sa.Column('item_total', sa.Integer(), nullable=True),
    sa.Column('state', sa.String(), nullable=True),
    sa.Column('email', sa.String(), nullable=True),
    sa.Column('last_ip_address', sa.String(), nullable=True),
    sa.Column('item_count', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('line_item',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('variant_id', sa.Integer(), nullable=True),
    sa.Column('ord_id', sa.Integer(), nullable=True),
    sa.Column('qty', sa.Integer(), nullable=True),
    sa.Column('price', sa.Numeric(scale=2), nullable=True),
    sa.Column('cost_price', sa.Numeric(scale=2), nullable=True),
    sa.ForeignKeyConstraint(['ord_id'], ['ord.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['variant_id'], ['variant.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('variant_id', 'ord_id', name='uniq_variant_ord_line_item')
    )
    op.add_column('vendor', sa.Column('branch_address_id', sa.Integer(), nullable=True))
    op.add_column('vendor', sa.Column('main_address_id', sa.Integer(), nullable=True))
    op.drop_column('vendor', 'mobile_phone')
    op.drop_column('vendor', 'contact_phone')
    op.drop_column('vendor', 'email')
    op.drop_column('vendor', 'main_address')
    op.drop_column('vendor', 'branch_address')


def downgrade():
    op.add_column('vendor', sa.Column('branch_address', sa.TEXT(), autoincrement=False, nullable=True))
    op.add_column('vendor', sa.Column('main_address', sa.TEXT(), autoincrement=False, nullable=True))
    op.add_column('vendor', sa.Column('email', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.add_column('vendor', sa.Column('contact_phone', sa.VARCHAR(length=20), autoincrement=False, nullable=True))
    op.add_column('vendor', sa.Column('mobile_phone', sa.VARCHAR(length=20), autoincrement=False, nullable=True))
    op.drop_column('vendor', 'main_address_id')
    op.drop_column('vendor', 'branch_address_id')
    op.drop_table('line_item')
    op.drop_table('ord')
    op.drop_table('address')
