"""add delete_flag for properties

Revision ID: 298d15b8a529
Revises: e90568c80ca8
Create Date: 2016-05-22 20:28:46.874620

"""

# revision identifiers, used by Alembic.
revision = '298d15b8a529'
down_revision = 'e90568c80ca8'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('prototype', sa.Column('delete_flag', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('prototype', 'delete_flag')
