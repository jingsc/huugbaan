"""create product styles

Revision ID: 2b0827cb9075
Revises: fd59f99231da
Create Date: 2016-10-29 16:50:47.910437

"""

# revision identifiers, used by Alembic.
revision = '2b0827cb9075'
down_revision = 'fd59f99231da'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('product_style',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=250), nullable=False),
    sa.Column('name_th', sa.String(length=250), nullable=True),
    sa.Column('disabled', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name'),
    sa.UniqueConstraint('name_th')
    )
    op.create_table('product_theme',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('product_id', sa.Integer(), nullable=True),
    sa.Column('product_style_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['product_id'], ['product.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['product_style_id'], ['product_style.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('product_style_id', 'product_id', name='uniq_psid_pid')
    )


def downgrade():
    op.drop_table('product_theme')
    op.drop_table('product_style')
