"""cascade delete

Revision ID: 26bdb7bc697c
Revises: d71a6abe924b
Create Date: 2016-08-27 13:34:26.874224

"""

# revision identifiers, used by Alembic.
revision = '26bdb7bc697c'
down_revision = 'd71a6abe924b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_constraint(u'vendor_address_address_id_fkey', 'vendor_address', type_='foreignkey')
    op.drop_constraint(u'vendor_address_vendor_id_fkey', 'vendor_address', type_='foreignkey')
    op.create_foreign_key(None, 'vendor_address', 'address', ['address_id'], ['id'], ondelete='CASCADE')
    op.create_foreign_key(None, 'vendor_address', 'vendor', ['vendor_id'], ['id'], ondelete='CASCADE')


def downgrade():
    op.drop_constraint(None, 'vendor_address', type_='foreignkey')
    op.drop_constraint(None, 'vendor_address', type_='foreignkey')
    op.create_foreign_key(u'vendor_address_vendor_id_fkey', 'vendor_address', 'vendor', ['vendor_id'], ['id'])
    op.create_foreign_key(u'vendor_address_address_id_fkey', 'vendor_address', 'address', ['address_id'], ['id'])
