"""add created_at to user_scene

Revision ID: ffced4226486
Revises: cf41153624cf
Create Date: 2016-06-03 16:54:49.988224

"""

# revision identifiers, used by Alembic.
revision = 'ffced4226486'
down_revision = 'cf41153624cf'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('user_scene', sa.Column('created_at', sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column('user_scene', 'created_at')
