"""add disabled for brand

Revision ID: d90e13f4d31a
Revises: c69b3b77e564
Create Date: 2016-08-07 13:12:10.290466

"""

# revision identifiers, used by Alembic.
revision = 'd90e13f4d31a'
down_revision = 'c69b3b77e564'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('brand', sa.Column('disabled', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('brand', 'disabled')
