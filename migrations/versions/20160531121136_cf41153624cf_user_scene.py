"""user scene

Revision ID: cf41153624cf
Revises: f039ea2a6c6b
Create Date: 2016-05-31 12:11:36.278473

"""

# revision identifiers, used by Alembic.
revision = 'cf41153624cf'
down_revision = 'f039ea2a6c6b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('user_scene',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('scene_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['scene_id'], ['scene.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id', 'scene_id', name='uniq_uid_sid')
    )


def downgrade():
    op.drop_table('user_scene')
