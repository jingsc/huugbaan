"""social auth

Revision ID: 763df671c376
Revises: caedc5d078a7
Create Date: 2016-05-01 13:40:30.507332

"""

# revision identifiers, used by Alembic.
revision = '763df671c376'
down_revision = 'caedc5d078a7'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.create_table('social_auth_usersocialauth',
    sa.Column('uid', sa.String(length=255)),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('provider', sa.String(length=32), nullable=True),
    sa.Column('extra_data', postgresql.JSONB()),
    sa.Column('user_id', sa.Integer()),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('provider', 'uid')
    )


def downgrade():
    op.drop_table('social_auth_usersocialauth')
