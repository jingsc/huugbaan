"""location stuff

Revision ID: eadbb4f998f7
Revises: 78815592b520
Create Date: 2016-10-02 13:15:50.355059

"""

# revision identifiers, used by Alembic.
revision = 'eadbb4f998f7'
down_revision = '78815592b520'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.add_column('scene', sa.Column('location_text', sa.String(length=150), nullable=True))
    op.drop_column('vendor', 'business_hours')


def downgrade():
    op.add_column('vendor', sa.Column('business_hours', postgresql.JSONB(), autoincrement=False, nullable=True))
    op.drop_column('scene', 'location_text')
