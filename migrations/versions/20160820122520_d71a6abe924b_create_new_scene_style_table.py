"""create new scene style table

Revision ID: d71a6abe924b
Revises: 52e52e5c0d27
Create Date: 2016-08-20 12:25:20.944203

"""

# revision identifiers, used by Alembic.
revision = 'd71a6abe924b'
down_revision = '52e52e5c0d27'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('scene_style',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=250), nullable=False),
    sa.Column('name_th', sa.String(length=250), nullable=True),
    sa.Column('disabled', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name'),
    sa.UniqueConstraint('name_th')
    )
    op.add_column('scene', sa.Column('style_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'scene', 'scene_style', ['style_id'], ['id'])
    op.drop_column('scene', 'style')


def downgrade():
    op.add_column('scene', sa.Column('style', sa.VARCHAR(length=150), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'scene', type_='foreignkey')
    op.drop_column('scene', 'style_id')
    op.drop_table('scene_style')
