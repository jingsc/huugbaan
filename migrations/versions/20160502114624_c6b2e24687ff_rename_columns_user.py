"""rename columns user

Revision ID: c6b2e24687ff
Revises: cfd976c64eec
Create Date: 2016-05-02 11:46:24.626224

"""

# revision identifiers, used by Alembic.
revision = 'c6b2e24687ff'
down_revision = 'cfd976c64eec'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_constraint(u'user_email_key', 'user', type_='unique')
    op.drop_constraint(u'user_name_key', 'user', type_='unique')


def downgrade():
    op.create_unique_constraint(u'user_name_key', 'user', ['name'])
    op.create_unique_constraint(u'user_email_key', 'user', ['email'])
