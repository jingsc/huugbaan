"""add more info columns in vendor

Revision ID: f039ea2a6c6b
Revises: 068874486577
Create Date: 2016-05-23 11:07:06.970006

"""

# revision identifiers, used by Alembic.
revision = 'f039ea2a6c6b'
down_revision = '068874486577'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('vendor', sa.Column('branch_adress', sa.Text(), nullable=True))
    op.add_column('vendor', sa.Column('contact_phone', sa.String(length=20), nullable=True))
    op.add_column('vendor', sa.Column('mobile_phone', sa.String(length=20), nullable=True))
    op.add_column('vendor', sa.Column('email', sa.String(length=100), nullable=True))
    op.add_column('vendor', sa.Column('main_adress', sa.Text(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('vendor', 'main_adress')
    op.drop_column('vendor', 'email')
    op.drop_column('vendor', 'contact_phone')
    op.drop_column('vendor', 'mobile_phone')
    op.drop_column('vendor', 'branch_adress')
