"""remove contrant

Revision ID: 8d37be98ad30
Revises: 4c0d4ba23155
Create Date: 2016-07-28 14:12:44.321310

"""

# revision identifiers, used by Alembic.
revision = '8d37be98ad30'
down_revision = '4c0d4ba23155'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_constraint(u'address_firstname_key', 'address', type_='unique')
    op.drop_constraint(u'address_lastname_key', 'address', type_='unique')


def downgrade():
    op.create_unique_constraint(u'address_lastname_key', 'address', ['lastname'])
    op.create_unique_constraint(u'address_firstname_key', 'address', ['firstname'])
