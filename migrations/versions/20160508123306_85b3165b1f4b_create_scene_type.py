"""create scene type

Revision ID: 85b3165b1f4b
Revises: ab2b7a81de4d
Create Date: 2016-05-08 12:33:06.837154

"""

# revision identifiers, used by Alembic.
revision = '85b3165b1f4b'
down_revision = 'ab2b7a81de4d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('scene_type',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.add_column('scene', sa.Column('scene_type_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'scene', 'scene_type', ['scene_type_id'], ['id'])


def downgrade():
    op.drop_constraint(None, 'scene', type_='foreignkey')
    op.drop_column('scene', 'scene_type_id')
    op.drop_table('scene_type')
