"""add ideabook_id to ideabookphoto

Revision ID: 52e52e5c0d27
Revises: 56be0950d526
Create Date: 2016-08-17 15:49:11.999042

"""

# revision identifiers, used by Alembic.
revision = '52e52e5c0d27'
down_revision = '56be0950d526'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('ideabook_photo', sa.Column('ideabook_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'ideabook_photo', 'ideabook', ['ideabook_id'], ['id'])


def downgrade():
    op.drop_constraint(None, 'ideabook_photo', type_='foreignkey')
    op.drop_column('ideabook_photo', 'ideabook_id')
