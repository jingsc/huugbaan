"""add business hours for vendor

Revision ID: 0e240c012ded
Revises: c5cd65bf7b75
Create Date: 2016-09-23 17:25:57.959846

"""

# revision identifiers, used by Alembic.
revision = '0e240c012ded'
down_revision = 'c5cd65bf7b75'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.add_column('vendor', sa.Column('business_hours', postgresql.JSONB(), nullable=True))


def downgrade():
    op.drop_column('vendor', 'business_hours')
