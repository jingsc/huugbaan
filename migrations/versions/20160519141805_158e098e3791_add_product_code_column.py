"""add product code column

Revision ID: 158e098e3791
Revises: 85b3165b1f4b
Create Date: 2016-05-19 14:18:05.753149

"""

# revision identifiers, used by Alembic.
revision = '158e098e3791'
down_revision = '85b3165b1f4b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('product', sa.Column('code', sa.String(length=50), nullable=True))


def downgrade():
    op.drop_column('product', 'code')
