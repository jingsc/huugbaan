"""add location and business hour for address

Revision ID: 78815592b520
Revises: 0e240c012ded
Create Date: 2016-10-02 12:36:06.953899

"""

# revision identifiers, used by Alembic.
revision = '78815592b520'
down_revision = '0e240c012ded'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.add_column('address', sa.Column('business_hours', postgresql.JSONB(), nullable=True))
    op.add_column('address', sa.Column('location', sa.String(length=150), nullable=True))
    op.drop_column('address', 'open_time')


def downgrade():
    op.add_column('address', sa.Column('open_time', sa.VARCHAR(length=50), autoincrement=False, nullable=True))
    op.drop_column('address', 'location')
    op.drop_column('address', 'business_hours')
