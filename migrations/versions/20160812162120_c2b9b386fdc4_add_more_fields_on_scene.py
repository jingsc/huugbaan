"""add more fields on scene

Revision ID: c2b9b386fdc4
Revises: 1dd81773b5ac
Create Date: 2016-08-12 16:21:20.514519

"""

# revision identifiers, used by Alembic.
revision = 'c2b9b386fdc4'
down_revision = '1dd81773b5ac'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('scene', sa.Column('designer', sa.String(length=250), nullable=True))
    op.add_column('scene', sa.Column('location', sa.String(length=150), nullable=True))
    op.add_column('scene', sa.Column('name_th', sa.String(length=250), nullable=True))
    op.add_column('scene', sa.Column('owner', sa.String(length=250), nullable=True))
    op.add_column('scene', sa.Column('photo_by', sa.String(length=250), nullable=True))
    op.add_column('scene', sa.Column('style', sa.String(length=150), nullable=True))
    op.alter_column('scene', 'name',
               existing_type=sa.VARCHAR(length=250),
               nullable=False)
    op.create_unique_constraint(None, 'scene', ['name_th'])
    op.add_column('scene_type', sa.Column('name_th', sa.String(), nullable=True))
    op.create_unique_constraint(None, 'scene_type', ['name_th'])


def downgrade():
    op.drop_constraint(None, 'scene_type', type_='unique')
    op.drop_column('scene_type', 'name_th')
    op.drop_constraint(None, 'scene', type_='unique')
    op.alter_column('scene', 'name',
               existing_type=sa.VARCHAR(length=250),
               nullable=True)
    op.drop_column('scene', 'style')
    op.drop_column('scene', 'photo_by')
    op.drop_column('scene', 'owner')
    op.drop_column('scene', 'name_th')
    op.drop_column('scene', 'location')
    op.drop_column('scene', 'designer')
