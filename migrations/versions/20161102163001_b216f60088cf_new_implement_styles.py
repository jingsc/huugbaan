"""new implement styles

Revision ID: b216f60088cf
Revises: 21dded62f7ca
Create Date: 2016-11-02 16:30:01.694251

"""

# revision identifiers, used by Alembic.
revision = 'b216f60088cf'
down_revision = '21dded62f7ca'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.create_table('style',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('created_by', sa.String(length=100), nullable=True),
    sa.Column('updated_by', sa.String(length=100), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=250), nullable=False),
    sa.Column('name_th', sa.String(length=250), nullable=True),
    sa.Column('disabled', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name'),
    sa.UniqueConstraint('name_th')
    )
    op.drop_table('product_theme')
    op.drop_table('scene_theme')
    op.add_column('product_style', sa.Column('product_id', sa.Integer(), nullable=True))
    op.add_column('product_style', sa.Column('style_id', sa.Integer(), nullable=True))
    op.create_unique_constraint('uniq_product_style', 'product_style', ['product_id', 'style_id'])
    op.drop_constraint(u'product_style_name_key', 'product_style', type_='unique')
    op.drop_constraint(u'product_style_name_th_key', 'product_style', type_='unique')
    op.create_foreign_key(None, 'product_style', 'product', ['product_id'], ['id'])
    op.create_foreign_key(None, 'product_style', 'style', ['style_id'], ['id'])
    op.drop_column('product_style', 'name')
    op.drop_column('product_style', 'created_at')
    op.drop_column('product_style', 'updated_at')
    op.drop_column('product_style', 'created_by')
    op.drop_column('product_style', 'name_th')
    op.drop_column('product_style', 'disabled')
    op.drop_column('product_style', 'updated_by')
    op.drop_constraint(u'scene_style_id_fkey', 'scene', type_='foreignkey')
    op.drop_column('scene', 'style_id')
    op.add_column('scene_style', sa.Column('scene_id', sa.Integer(), nullable=True))
    op.add_column('scene_style', sa.Column('style_id', sa.Integer(), nullable=True))
    op.create_unique_constraint('uniq_scene_style', 'scene_style', ['scene_id', 'style_id'])
    op.drop_constraint(u'scene_style_name_key', 'scene_style', type_='unique')
    op.drop_constraint(u'scene_style_name_th_key', 'scene_style', type_='unique')
    op.create_foreign_key(None, 'scene_style', 'scene', ['scene_id'], ['id'])
    op.create_foreign_key(None, 'scene_style', 'style', ['style_id'], ['id'])
    op.drop_column('scene_style', 'name')
    op.drop_column('scene_style', 'created_at')
    op.drop_column('scene_style', 'updated_at')
    op.drop_column('scene_style', 'created_by')
    op.drop_column('scene_style', 'name_th')
    op.drop_column('scene_style', 'disabled')
    op.drop_column('scene_style', 'updated_by')


def downgrade():
    op.add_column('scene_style', sa.Column('updated_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.add_column('scene_style', sa.Column('disabled', sa.BOOLEAN(), autoincrement=False, nullable=True))
    op.add_column('scene_style', sa.Column('name_th', sa.VARCHAR(length=150), autoincrement=False, nullable=True))
    op.add_column('scene_style', sa.Column('created_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.add_column('scene_style', sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False))
    op.add_column('scene_style', sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False))
    op.add_column('scene_style', sa.Column('name', sa.VARCHAR(length=50), autoincrement=False, nullable=False))
    op.drop_constraint(None, 'scene_style', type_='foreignkey')
    op.drop_constraint(None, 'scene_style', type_='foreignkey')
    op.create_unique_constraint(u'scene_style_name_th_key', 'scene_style', ['name_th'])
    op.create_unique_constraint(u'scene_style_name_key', 'scene_style', ['name'])
    op.drop_constraint('uniq_scene_style', 'scene_style', type_='unique')
    op.drop_column('scene_style', 'style_id')
    op.drop_column('scene_style', 'scene_id')
    op.add_column('scene', sa.Column('style_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.create_foreign_key(u'scene_style_id_fkey', 'scene', 'scene_style', ['style_id'], ['id'])
    op.add_column('product_style', sa.Column('updated_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.add_column('product_style', sa.Column('disabled', sa.BOOLEAN(), autoincrement=False, nullable=True))
    op.add_column('product_style', sa.Column('name_th', sa.VARCHAR(length=250), autoincrement=False, nullable=True))
    op.add_column('product_style', sa.Column('created_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.add_column('product_style', sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False))
    op.add_column('product_style', sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False))
    op.add_column('product_style', sa.Column('name', sa.VARCHAR(length=250), autoincrement=False, nullable=False))
    op.drop_constraint(None, 'product_style', type_='foreignkey')
    op.drop_constraint(None, 'product_style', type_='foreignkey')
    op.create_unique_constraint(u'product_style_name_th_key', 'product_style', ['name_th'])
    op.create_unique_constraint(u'product_style_name_key', 'product_style', ['name'])
    op.drop_constraint('uniq_product_style', 'product_style', type_='unique')
    op.drop_column('product_style', 'style_id')
    op.drop_column('product_style', 'product_id')
    op.create_table('social_auth_usersocialauth',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('provider', sa.VARCHAR(length=32), autoincrement=False, nullable=True),
    sa.Column('extra_data', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('uid', sa.VARCHAR(length=255), autoincrement=False, nullable=True),
    sa.Column('user_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.ForeignKeyConstraint(['user_id'], [u'user.id'], name=u'social_auth_usersocialauth_user_id_fkey'),
    sa.PrimaryKeyConstraint('id', name=u'social_auth_usersocialauth_pkey')
    )
    op.create_table('scene_theme',
    sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('scene_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('scene_style_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['scene_id'], [u'scene.id'], name=u'scene_theme_scene_id_fkey', ondelete=u'CASCADE'),
    sa.ForeignKeyConstraint(['scene_style_id'], [u'scene_style.id'], name=u'scene_theme_scene_style_id_fkey', ondelete=u'CASCADE'),
    sa.PrimaryKeyConstraint('id', name=u'scene_theme_pkey'),
    sa.UniqueConstraint('scene_style_id', 'scene_id', name=u'uniq_ssid_sid')
    )
    op.create_table('product_theme',
    sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('product_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('product_style_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['product_id'], [u'product.id'], name=u'product_theme_product_id_fkey', ondelete=u'CASCADE'),
    sa.ForeignKeyConstraint(['product_style_id'], [u'product_style.id'], name=u'product_theme_product_style_id_fkey', ondelete=u'CASCADE'),
    sa.PrimaryKeyConstraint('id', name=u'product_theme_pkey'),
    sa.UniqueConstraint('product_style_id', 'product_id', name=u'uniq_psid_pid')
    )
    op.drop_table('style')
