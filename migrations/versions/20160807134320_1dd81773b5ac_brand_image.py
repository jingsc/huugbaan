"""brand image

Revision ID: 1dd81773b5ac
Revises: d90e13f4d31a
Create Date: 2016-08-07 13:43:20.746847

"""

# revision identifiers, used by Alembic.
revision = '1dd81773b5ac'
down_revision = 'd90e13f4d31a'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('brand', sa.Column('image', sa.String(), nullable=True))


def downgrade():
    op.drop_column('brand', 'image')
