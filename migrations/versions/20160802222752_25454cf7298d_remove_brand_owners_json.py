"""remove brand owners json

Revision ID: 25454cf7298d
Revises: 8d37be98ad30
Create Date: 2016-08-02 22:27:52.747877

"""

# revision identifiers, used by Alembic.
revision = '25454cf7298d'
down_revision = '8d37be98ad30'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.drop_column('vendor', 'brand_owners')


def downgrade():
    op.add_column('vendor', sa.Column('brand_owners', postgresql.JSONB(), autoincrement=False, nullable=True))
