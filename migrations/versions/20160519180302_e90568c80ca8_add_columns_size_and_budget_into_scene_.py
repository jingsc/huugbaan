"""add columns size and budget into scene table

Revision ID: e90568c80ca8
Revises: 158e098e3791
Create Date: 2016-05-19 18:03:02.127237

"""

# revision identifiers, used by Alembic.
revision = 'e90568c80ca8'
down_revision = '158e098e3791'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_unique_constraint(None, 'product', ['code'])
    op.add_column('scene', sa.Column('budget_type', sa.String(length=3), nullable=True))
    op.add_column('scene', sa.Column('size', sa.String(length=3), nullable=True))


def downgrade():
    op.drop_column('scene', 'size')
    op.drop_column('scene', 'budget_type')
    op.drop_constraint(None, 'product', type_='unique')
