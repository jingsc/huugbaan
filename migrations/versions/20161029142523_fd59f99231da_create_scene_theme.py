"""create scene theme

Revision ID: fd59f99231da
Revises: 7ddf158e93e1
Create Date: 2016-10-29 14:25:23.366268

"""

# revision identifiers, used by Alembic.
revision = 'fd59f99231da'
down_revision = '7ddf158e93e1'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('scene_theme',
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('scene_id', sa.Integer(), nullable=True),
    sa.Column('scene_style_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['scene_id'], ['scene.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['scene_style_id'], ['scene_style.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('scene_style_id', 'scene_id', name='uniq_ssid_sid')
    )


def downgrade():
    op.drop_table('scene_theme')
