"""generic address

Revision ID: c5cd65bf7b75
Revises: 26bdb7bc697c
Create Date: 2016-08-28 15:53:33.301893

"""

# revision identifiers, used by Alembic.
revision = 'c5cd65bf7b75'
down_revision = '26bdb7bc697c'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    op.drop_table('user_address')
    op.drop_table('vendor_address')
    op.add_column('address', sa.Column('discriminator', sa.String(), nullable=True))
    op.add_column('address', sa.Column('parent_id', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('address', 'parent_id')
    op.drop_column('address', 'discriminator')
    op.create_table('vendor_address',
    sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('created_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('updated_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('address_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('vendor_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['address_id'], [u'address.id'], name=u'vendor_address_address_id_fkey', ondelete=u'CASCADE'),
    sa.ForeignKeyConstraint(['vendor_id'], [u'vendor.id'], name=u'vendor_address_vendor_id_fkey', ondelete=u'CASCADE'),
    sa.PrimaryKeyConstraint('id', name=u'vendor_address_pkey'),
    sa.UniqueConstraint('address_id', 'vendor_id', name=u'uniq_address_vendor')
    )
    op.create_table('user_address',
    sa.Column('created_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('created_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('updated_by', sa.VARCHAR(length=100), autoincrement=False, nullable=True),
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('address_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('user_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['address_id'], [u'address.id'], name=u'user_address_address_id_fkey'),
    sa.ForeignKeyConstraint(['user_id'], [u'user.id'], name=u'user_address_user_id_fkey'),
    sa.PrimaryKeyConstraint('id', name=u'user_address_pkey'),
    sa.UniqueConstraint('address_id', 'user_id', name=u'uniq_address_user')
    )
