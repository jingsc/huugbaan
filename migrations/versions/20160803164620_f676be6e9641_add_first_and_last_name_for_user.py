"""add first and last name for user

Revision ID: f676be6e9641
Revises: 25454cf7298d
Create Date: 2016-08-03 16:46:20.272519

"""

# revision identifiers, used by Alembic.
revision = 'f676be6e9641'
down_revision = '25454cf7298d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('user', sa.Column('first_name', sa.String(length=100), nullable=True))
    op.add_column('user', sa.Column('last_name', sa.String(length=100), nullable=True))


def downgrade():
    op.drop_column('user', 'last_name')
    op.drop_column('user', 'first_name')
