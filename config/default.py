# coding: utf-8
import os


class Config(object):
    """Base config class."""
    # Flask app config
    DEBUG = False
    TESTING = False
    SECRET_KEY = "\xb5\xb3}#\xb7A\xcac\x9d0\xb6\x0f\x80z\x97\x00\x1e\xc0\xb8+\xe9)\xf0}"
    PERMANENT_SESSION_LIFETIME = 3600 * 24 * 7
    SESSION_COOKIE_NAME = 'huugbaan_session'

    # Root path of project
    PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    # Site domain
    SITE_TITLE = "HUUG.BAAN"
    SITE_DOMAIN = "huugbaan.com"

    # SQLAlchemy config
    # See:
    # https://pythonhosted.org/Flask-SQLAlchemy/config.html#connection-uri-format
    # http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html#database-urls
    # SQLALCHEMY_DATABASE_URI = "mysql+pymysql://user:password@host/database"
    SQLALCHEMY_DATABASE_URI = "postgresql://jing:@localhost/huugbaan"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SOCIAL AUTH ------------------------------------------
    SOCIAL_AUTH_LOGIN_URL = '/login'
    SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
    SOCIAL_AUTH_USER_MODEL = 'application.models.User'
    SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
        'social.backends.google.GoogleOpenId',
        'social.backends.google.GoogleOAuth2',
        'social.backends.google.GoogleOAuth',
        'social.backends.google.GooglePlusAuth',
        'social.backends.facebook.FacebookOAuth2',
        'social.backends.facebook.FacebookAppOAuth2',
        'social.backends.twitter.TwitterOAuth',
    )

    SOCIAL_AUTH_PIPELINE = (
        'social.pipeline.social_auth.social_details',
        'social.pipeline.social_auth.social_uid',
        'social.pipeline.social_auth.auth_allowed',
        'social.pipeline.social_auth.social_user',
        'social.pipeline.user.get_username',
        'social.pipeline.user.create_user',
        'social.pipeline.social_auth.associate_user',
        'social.pipeline.social_auth.load_extra_data',
        'social.pipeline.user.user_details',
        'application.models._util.save_more_user_info', # !! hook for save more user data
    )

    SOCIAL_AUTH_FACEBOOK_KEY = "787228571412680"
    SOCIAL_AUTH_FACEBOOK_SECRET = "8e79496886ea2ac453d822b4b0e95b19"
    SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = "460413445132-20k9nsaman73qnv9s0qtf03qbe4414av.apps.googleusercontent.com"
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "2GT-9ISAw54rHX0YnOrkpvU0"
    SOCIAL_AUTH_GOOGLE_OAUTH_KEY = "460413445132-20k9nsaman73qnv9s0qtf03qbe4414av.apps.googleusercontent.com"
    SOCIAL_AUTH_GOOGLE_OAUTH_SECRET = "2GT-9ISAw54rHX0YnOrkpvU0"
    SOCIAL_AUTH_GOOGLE_KEY = "460413445132-20k9nsaman73qnv9s0qtf03qbe4414av.apps.googleusercontent.com"
    SOCIAL_AUTH_GOOGLE_SECRET = "2GT-9ISAw54rHX0YnOrkpvU0"
    SOCIAL_AUTH_GOOGLE_PLUS_AUTH_KEY = "460413445132-20k9nsaman73qnv9s0qtf03qbe4414av.apps.googleusercontent.com"
    SOCIAL_AUTH_GOOGLE_PLUS_AUTH_SECRET = "2GT-9ISAw54rHX0YnOrkpvU0"
    SOCIAL_AUTH_GOOGLE_OAUTH2_USE_DEPRECATED_API = True
    SOCIAL_AUTH_GOOGLE_PLUS_USE_DEPRECATED_API = True
    ##########################################################

    # Flask-DebugToolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    # Sentry config
    SENTRY_DSN = ''

    # Host string, used by fabric
    HOST_STRING = "root@huugbaan"
