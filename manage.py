# coding: utf-8
import os
import glob2
import application.models as models
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from application import create_app
from application.models import db

# Used by app debug & livereload
PORT = 5000

app = create_app()
manager = Manager(app)

# db migrate commands
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

def _make_shell_context():
    return dict(app=app, db=db, m=models)

manager.add_command('shell', Shell(make_context=_make_shell_context))

@manager.command
def run():
    """Run app."""
    app.run(host="0.0.0.0", port=PORT)


@manager.command
def live():
    """Run livereload server"""
    from livereload import Server

    server = Server(app)

    map(server.watch, glob2.glob("application/pages/**/*.*"))  # pages
    map(server.watch, glob2.glob("application/macros/**/*.html"))  # macros
    map(server.watch, glob2.glob("application/static/**/*.*"))  # public assets

    server.serve(port=PORT)


@manager.command
def build():
    """Use FIS to compile assets."""
    os.system('gulp')
    os.chdir('application')
    os.system('fis release -d ../output -opmD')


@manager.command
def init_social_auth_db():
    from application.models import db
    from social.apps.flask_app.default import models
    models.PSABase.metadata.create_all(db.engine)


# @manager.command
@manager.option("-path", "--path_to_layout")
def pack_css(path_to_layout):
    """
    packing css
    # 1. read main layout
    # 2. extract css path in order
    # 3. concat all css files into a single file
    # 4. create the concat file into a dir
    # 5. write <link css> back into main layout
    """
    import re
    abs_path = os.path.abspath(path_to_layout)
    if os.path.exists(abs_path):
        # print "path exist", path_to_layout
        css_paths = [] # /static/css/lib/
        with open(abs_path) as f:
            regexp_link_tag = r'^<link>'
            for line in f:
                l = line.strip()
                s = re.search("^<link.*href=.(.*\.css).*", l)
                if s:
                    match_text = s.group(0)
                    extracted_path = s.group(1)
                    css_paths.append(extracted_path)
        # TODO parameter for xxx
        dest = "application/static/build/prod.css"
        with open(dest, 'w') as o:
            for css_path in css_paths:
                css_dir = os.path.dirname(css_path)
                css_file = os.path.basename(css_path)
                for root, dirs, files in os.walk(os.getcwd()):
                    if css_dir in root and css_file in files:
                        abs_css_file = os.path.join(root, css_file)
                        with open(os.path.join(root, css_file), 'r') as f:
                            o.write(f.read())
                            f.close()
            o.close()
        print "successfully pack css to %s" % dest
    else:
        print "not found %s" % abs_path

@manager.option("-path", "--path_to_layout")
def pack_js(path_to_layout):
    """
    packing css
    # 1. read main layout
    # 2. extract css path in order
    # 3. concat all css files into a single file
    # 4. create the concat file into a dir
    # 5. write <link css> back into main layout
    """
    import re
    abs_path = os.path.abspath(path_to_layout)
    if os.path.exists(abs_path):
        # print "path exist", path_to_layout
        css_paths = [] # /static/css/lib/
        with open(abs_path) as f:
            for line in f:
                l = line.strip()
                s = re.search("^<script.*src=.(.*\.js).*<\/script>", l)
                if s:
                    match_text = s.group(0)
                    extracted_path = s.group(1)
                    css_paths.append(extracted_path)
        # TODO parameter for xxx
        dest = "application/static/build/prod.js"
        with open(dest, 'w') as o:
            for css_path in css_paths:
                css_dir = os.path.dirname(css_path)
                css_file = os.path.basename(css_path)
                for root, dirs, files in os.walk(os.getcwd()):
                    if css_dir in root and css_file in files:
                        abs_css_file = os.path.join(root, css_file)
                        with open(os.path.join(root, css_file), 'r') as f:
                            o.write(f.read())
                            f.close()
            o.close()
        print "successfully pack css to %s" % dest
    else:
        print "not found %s" % abs_path

@manager.command
def pack_asset():
    # targeted files
    js_libs = ( "/static/js/libs/respond.min.js",
                "/static/js/libs/underscore-min.js",
                "/static/js/libs/jquery.min.js",
                "/static/js/libs/jquery.tooltipster.js",
                "/static/js/libs/jBox.min.js",
                "/static/js/libs/trix.js",
                "/static/js/jquery.taggd.js",
                "/static/js/jssocials.min.js",
                "/static/js/libs/unslider-min.js",)

    css_libs = ( "/static/css/font-awesome.min.css",
                 "/static/css/libs/normalize.css",
                 "/static/css/libs/tooltipster.css",
                 "/static/css/libs/tooltipster-shadow.css",
                 "/static/css/libs/trix.css",
                 "/static/css/libs/unslider.css",
                 "/static/css/jssocials.css",
                 "/static/css/jBox.css",
                 "/static/css/taggd.css",
                 "/static/css/jssocials-theme-classic.css",
                 "/static/css/bulma.css",
            )

    # destination file
    dest_js = "application/static/build/prod.js"
    dest_css = "application/static/build/prod.css"

    # packing process
    for dest, asset_files in [(dest_js, js_libs), (dest_css, css_libs)]:
        with open(dest, 'w') as o:
            for asset_path in asset_files:
                _dir = os.path.dirname(asset_path)
                _filename = os.path.basename(asset_path)
                for root, dirs, files in os.walk(os.getcwd()):
                    if _dir in root and _filename in files:
                        abs_file_path = os.path.join(root, _filename)
                        with open(abs_file_path, 'r') as f:
                            o.write(f.read())
                            o.write("\n")
                            f.close()
            o.close()
    print "packed asset successful!"
    print "Javascript: %s" % dest_js
    print "CSS: %s" % dest_css
    print "========================" 

if __name__ == "__main__":
    manager.run()
