import datetime
from sqlalchemy import event
from sqlalchemy.orm import validates
from flask_sqlalchemy import SQLAlchemy
from flask_login import current_user

db = SQLAlchemy()


def auto_timestamp(cls):
    @event.listens_for(cls, 'before_insert')
    def add_created_at(mapper, connection, target):
        target.created_at = datetime.datetime.now()
        if target.updated_at is None:
            target.updated_at = datetime.datetime.now()

    @event.listens_for(cls, 'before_update')
    def add_updated_at(mapper, connection, target):
        target.updated_at = datetime.datetime.now()


class MetaTimestampEvent(type):
    def __init__(cls, name, bases, clsdict):
        auto_timestamp(cls)
        super(MetaTimestampEvent, cls).__init__(name, bases, clsdict)


class TimestampMixin(object):
    created_at = db.Column(db.DateTime, nullable=False, default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, default=db.func.now(), onupdate=db.func.now())


def auto_userstamp(cls):
    @event.listens_for(cls, 'before_insert')
    def add_userstamp_created_by(mapper, connection, target):
        if current_user.is_authenticated:
            target.created_by = current_user.username
            if target.updated_by is None:
                target.updated_by = current_user.username

    @event.listens_for(cls, 'before_update')
    def add_userstamp_updated_by(mapper, connection, target):
        if current_user.is_authenticated:
            target.updated_by = current_user.username


class MetaUserstampEvent(type):
    def __init__(cls, name, bases, clsdict):
        auto_userstamp(cls)
        super(MetaUserstampEvent, cls).__init__(name, bases, clsdict)


class UserstampMixin(object):
    created_by = db.Column(db.String(100))
    updated_by = db.Column(db.String(100))


# util method for all models
import copy
def attrs(self):
    attrs = copy.copy(self.__dict__)
    if '_sa_instance_state' in attrs:
        del attrs['_sa_instance_state']
    return attrs


setattr(db.Model, 'attrs', attrs)
