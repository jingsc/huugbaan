# coding: utf-8
import sqlalchemy as sa
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp
from sqlalchemy.dialects.postgresql import JSONB


class Address(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    firstname = db.Column(db.Unicode(200))
    lastname = db.Column(db.Unicode(200))

    address1 = db.Column(db.Text)
    address2 = db.Column(db.Text)

    district = db.Column(db.String(50))
    city = db.Column(db.String(50))
    province = db.Column(db.String(50))
    zipcode = db.Column(db.String(10))

    phone = db.Column(db.String(20))
    alternative_phone = db.Column(db.String(20))

    line_id = db.Column(db.String(50))
    # open_time = db.Column(db.String(50))
    company = db.Column(db.Unicode(100))
    email = db.Column(db.String(100))
    location = db.Column(db.String(150))
    business_hours = db.Column(JSONB) # ['day': 'time']

    # state for vendor
    is_main = db.Column(db.Boolean)
    in_used = db.Column(db.Boolean)

    # generic references
    discriminator = db.Column(db.String)
    parent_id = db.Column(db.Integer)

    @property
    def parent(self):
        return getattr(self, "parent_%s", self.discriminator)

    def __repr__(self):
        return '<Address %s>' % self.line_id

    # def __unicode__(self):
    #     main = "<Main>" if self.is_main else "<Branch>"
    #     display = '{main}Address [ Tel:{phone} - Email:{email} ]'.format(main=main, phone=self.phone, email=self.email)
    #     return display


auto_userstamp(Address)
auto_timestamp(Address)


class HasAddressesMixin(object):
    """Mixin class for create a relationship to the
    address_association table for each parent"""


@sa.event.listens_for(HasAddressesMixin, "mapper_configured", propagate=True)
def setup_listener(mapper, class_):
    name = class_.__name__
    discriminator = name.lower()
    class_.addresses = db.relationship(Address,
                            primaryjoin=sa.and_(
                                    class_.id == sa.orm.foreign(sa.orm.remote(Address.parent_id)),
                                    Address.discriminator == discriminator
                                ),
                            backref=db.backref(
                                    "parent_%s" % discriminator,
                                    primaryjoin=sa.orm.remote(class_.id) == sa.orm.foreign(Address.parent_id)
                                )
                            )

    @sa.event.listens_for(class_.addresses, "append")
    def append_address(target, value, initiator):
        value.discriminator = discriminator
