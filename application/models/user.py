# coding: utf-8
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, UserstampMixin, auto_userstamp, auto_timestamp
from flask_login import UserMixin, current_user
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy import event
from .ideabook import Ideabook


class User(db.Model, UserMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(200))
    password = db.Column(db.String(200), default='')
    username = db.Column(db.String(200))
    active = db.Column(db.Boolean(), default=True)

    is_admin = db.Column(db.Boolean, default=False)
    avatar = db.Column(db.String(200))
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now)

    scenes = association_proxy('user_scenes', 'scene')

    def get_avatar(self):
        return self.avatar or "/static/image/default.jpg"

    def has_scene(self, scene_id):
        if scene_id is not None:
            us = filter(lambda r: r.scene_id == scene_id, self.user_scenes)
            if len(us) > 0:
                return True
        return False

    def has_product_in_ideabook(self, product_id):
        if product_id is not None:
            # TODO
            return False
        return False

    def __setattr__(self, name, value):
        # Hash password when set it.
        if name == 'password':
            value = generate_password_hash(value)
        super(User, self).__setattr__(name, value)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @property
    def is_active(self):
        return self.active

    @is_active.setter
    def is_active(self, value):
        self.active = value

    @property
    def cart_order(self):
        res = filter(lambda ord: ord.state == 'cart', self.orders)
        res = res and res[0]
        if res:
            return res
        return None

    def get_item_count_in_cart(self):
        if self.cart_order:
            return self.cart_order.item_count
        return 0

    def create_ideabook(self, name):
        ib = Ideabook(name=name, user_id=self.id)
        db.session.add(ib)
        db.session.commit()
        return ib

    def __repr__(self):
        return '<User %s>' % self.name


auto_userstamp(User)
auto_timestamp(User)
class UserScene(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    scene_id = db.Column(db.Integer, db.ForeignKey('scene.id', ondelete='CASCADE'))
    created_at = db.Column(db.DateTime, default=datetime.now)

    __table_args__ = ( db.UniqueConstraint('user_id', 'scene_id', name='uniq_uid_sid'), )
    user = db.relationship('User', backref=db.backref('user_scenes', cascade='all,delete-orphan', single_parent=True), uselist=False)
    scene = db.relationship('Scene', backref=db.backref('user_scenes',cascade='all,delete-orphan', single_parent=True), uselist=False)
