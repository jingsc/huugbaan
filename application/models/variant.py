# coding: utf-8
from datetime import datetime
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp


class Variant(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    sku = db.Column(db.String(50))
    # weight = db.Column(db.Numeric(scale=2))
    # height = db.Column(db.Numeric(scale=2))
    # width = db.Column(db.Numeric(scale=2))
    # depth = db.Column(db.Numeric(scale=2))
    deleted_at = db.Column(db.DateTime)
    is_master = db.Column(db.Boolean, default=False)
    reference_url = db.Column(db.String)

    # relationship
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    vendor_id = db.Column(db.Integer, db.ForeignKey('vendor.id'))

    __table_args__ = ( db.UniqueConstraint('product_id', 'vendor_id', name='uniq_variant_product_vendor'), )

    product = db.relationship("Product", backref=db.backref("variants"))
    vendor = db.relationship("Vendor", backref=db.backref("variants"))

    # financial
    price = db.Column(db.Numeric(scale=2))

    def __repr__(self):
        return '<Variant %s>' % self.sku


auto_userstamp(Variant)
auto_timestamp(Variant)
