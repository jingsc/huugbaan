# coding: utf-8
from ._base import db, TimestampMixin, UserstampMixin, \
                   auto_userstamp, auto_timestamp


class Brand(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name_en = db.Column(db.Unicode(50), unique=True)
    name_th = db.Column(db.Unicode(50), unique=True)
    presentation_en = db.Column(db.Unicode(200), unique=True)
    presentation_th = db.Column(db.Unicode(200), unique=True)
    disabled = db.Column(db.Boolean, default=False)
    image = db.Column(db.String())

    products = db.relationship('Product', backref=db.backref('brand'))

    def __repr__(self):
        return self.name_en
        return '<Brand %s>' % self.name_en


    def __unicode__(self):
        return self.name_en


auto_userstamp(Brand)
auto_timestamp(Brand)


class VendorBrand(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    # relationship
    brand_id = db.Column(db.Integer, db.ForeignKey('brand.id'))
    vendor_id = db.Column(db.Integer, db.ForeignKey('vendor.id'))

    __table_args__ = ( db.UniqueConstraint('brand_id', 'vendor_id', name='uniq_brand_vendor'), )

    brand = db.relationship("Brand", backref=db.backref("vendor_brand"))
    vendor = db.relationship("Vendor", backref=db.backref("vendor_brand"))


auto_userstamp(VendorBrand)
auto_timestamp(VendorBrand)
