# coding: utf-8
from flask import url_for
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp
from .product import Product
from .scene import Scene
# from sqlalchemy.dialects.postgresql import JSONB


class Ideabook(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, unique=True)
    # belongs to user
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    # relationship
    user = db.relationship("User", backref=db.backref("ideabooks"))


auto_userstamp(Ideabook)
auto_timestamp(Ideabook)


class IdeabookPhoto(db.Model, TimestampMixin):
    id = db.Column(db.Integer, primary_key=True)
    assoc_type = db.Column(db.String(), nullable=False) # scene or photo
    ideabook_id = db.Column(db.Integer, db.ForeignKey('ideabook.id'))
    photo_id = db.Column(db.Integer) # scene or photo
    notes = db.Column(db.Text)

    ideabook = db.relationship("Ideabook", backref=db.backref("photos"))
    # make comment private

    @property
    def img_url(self):
        if self.assoc_type == 'product':
            p = Product.query.get(self.photo_id)
            return p.master_image["uri"]
        if self.assoc_type == 'scene':
            return "/scene_images/%s" % self.item.uri

    @property
    def item(self):
        if self.assoc_type == 'product':
            p = Product.query.get(self.photo_id)
            return p
        if self.assoc_type == 'scene':
            p = Scene.query.get(self.photo_id)
            return p

    @property
    def photo_name(self):
        if self.assoc_type == 'product':
            p = Product.query.get(self.photo_id)
            return p
        if self.assoc_type == 'scene':
            p = Scene.query.get(self.photo_id)
            return p
        pass


    def get_link(self):
        if self.assoc_type == 'product':
            return url_for("product_controller.show", product_id=self.photo_id)
        if self.assoc_type == 'scene':
            return url_for("scene_controller.show", sid=self.photo_id)


auto_timestamp(IdeabookPhoto)
