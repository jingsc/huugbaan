import random


def save_more_user_info(backend, user, response, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = "http://graph.facebook.com/%s/picture?type=large"%response['id']
    # if backend.name == 'twitter':
    #     url = response.get('profile_image_url', '').replace('_normal','')
    if backend.name == 'google-oauth2':
        url = response['picture']
        ext = url.split('.')[-1]
    if url:
        user.avatar = url
        user.query.session.commit()

'''
# example format of google-oauth2
{
     u'family_name': u'chobpanich', 
     u'name': u'sarunyoo chobpanich', 
     u'picture': u'https://lh6.googleusercontent.com/-bqiu8R6ESl0/AAAAAAAAAAI/AAAAAAAAARs/5PZ7VK7oDU0/photo.jpg', 
     u'access_token': u'pa6Md1tvcsOhAyQondSCGzbaDA', 
     u'expires_in': 3599, 
     u'id': u'106782313557676957341', 
     u'token_type': u'Bearer', 
     u'locale': u'en', 
     u'link': u'https://plus.google.com/106782313557676957341', 
     u'given_name': u'sarunyoo', 
     u'gender': u'male', 
     u'email': u'wsaryoo@gmail.com', 
     u'verified_email': True 
 }

# example format of facebook
{
    'access_token': u'EAALLZBu9bKMgBAOwCoCP0ut',
    'name': u'Sarunyoo Chobpanich', 
    'id': u'1345351398813455'
}
'''


def sample_from_query(qry, n):
    """ sample records from query """

    if qry.count() > n:
        offset = random.sample(range(1, qry.count()), 1)[0]
        offset = offset - n
        if offset < 0: 
            offset = 0
        return qry.offset(offset).limit(n)
    else:
        return qry.all()
