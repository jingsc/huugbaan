# coding: utf-8
import random
from datetime import datetime
from .searchable_mixin import SearchableMixin
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp
from ._util import sample_from_query
from sqlalchemy.ext.associationproxy import association_proxy
from .product import Product
from .prototype import Prototype
from .style import Style


class SceneType(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    name_th = db.Column(db.String, unique=True)
    disabled = db.Column(db.Boolean, default=False)

    def sample_scenes(self, n):
        if len(self.scenes) > 0:
            if n > len(self.scenes):
                _n = len(self.scenes)
            else:
                _n = n
            _sample = random.sample(self.scenes, _n)
            random.shuffle(_sample)
            return _sample
        return []

    def __repr__(self):
        return self.name


auto_userstamp(SceneType)
auto_timestamp(SceneType)


class SceneProduct(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id', ondelete='CASCADE'))
    scene_id = db.Column(db.Integer, db.ForeignKey('scene.id', ondelete='CASCADE'))
    pos_x = db.Column(db.Integer) # < 100
    pos_y = db.Column(db.Integer)

    __table_args__ = ( db.UniqueConstraint('product_id', 'scene_id', name='uniq_pid_sid'), )
    product = db.relationship('Product', backref=db.backref('scenes_products', cascade='all,delete-orphan', single_parent=True), uselist=False)
    scene = db.relationship('Scene', backref=db.backref('scenes_products',cascade='all,delete-orphan', single_parent=True), uselist=False)


auto_userstamp(SceneProduct)
auto_timestamp(SceneProduct)


class Scene(db.Model, TimestampMixin, UserstampMixin, SearchableMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name = db.Column(db.String(250), unique=True, nullable=False)
    name_th = db.Column(db.String(250), unique=True)
    description = db.Column(db.Text)
    uri = db.Column(db.String(300), unique=True, nullable=False)
    scene_type_id = db.Column(db.Integer, db.ForeignKey('scene_type.id'))
    size = db.Column(db.String(3)) # S M L XL
    budget_type = db.Column(db.String(3)) # $, $$, $$$
    # style_id = db.Column(db.Integer, db.ForeignKey('scene_style.id'))

    # tracker
    location = db.Column(db.String(150)) # geo location
    location_text = db.Column(db.String(150)) # text location
    owner = db.Column(db.String(250))
    designer = db.Column(db.String(250))
    photo_by = db.Column(db.String(250))

    products = association_proxy('scenes_products', 'product')
    styles = db.relationship('Style', 
                              secondary='scene_style', 
                              backref=db.backref('scenes'))

    users = association_proxy('user_scenes', 'scene')
    scene_type = db.relationship('SceneType', backref=db.backref('scenes'))
    # scene_style = db.relationship('SceneStyle', backref=db.backref('scenes'))

    # relationship
    # products = db.relationship('Product', 
    #                             secondary='scene_product', 
    #                             backref=db.backref('scenes'))

    @property
    def related_products(self, n=6):
        return self.get_related_products(n)

    def get_related_products(self, n=6):
        pid = [p.id for p in self.products ]
        ptid = [p.prototype_id for p in self.products ]
        qry = Product.query.filter(Prototype.id.in_(ptid)).filter(~Product.id.in_(pid))
        return sample_from_query(qry, n)

    @property
    def related_scenes(self, n=6):
        return self.get_related_scenes(n)

    def get_related_scenes(self, n=6):
        stid = [st.id for st in self.styles] or []
        if stid:
            qry = Scene.query.filter(Style.id.in_(stid)).filter(Scene.id != self.id)
        else:
            return []
        return sample_from_query(qry, n)

    def __repr__(self):
        return '<Scene %s>' % self.name

    def thumb_uri(self):
        if self.uri:
            a = self.uri.split('.') 
            return "%s_thumb.%s" % (a[0], a[1])
        else:
            return None


auto_userstamp(Scene)
auto_timestamp(Scene)
