# coding: utf-8
from datetime import datetime
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy import event


class Ord(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    item_total = db.Column(db.Integer, default=1)

    # format R[0-9]{9}
    number = db.Column(db.String(32))
    item_total = db.Column(db.Integer)

    # state flows :  TODO
    # cart -> address -> delivery -> 
    # payment -> confirm -> complete
    state = db.Column(db.String)
    email = db.Column(db.String)
    last_ip_address = db.Column(db.String)
    item_count = db.Column(db.Integer)

    # relationship
    user = db.relationship('User', backref=db.backref('orders', cascade='all,delete-orphan', single_parent=True), uselist=False)

    def get_total_by_vendor(self):
        summary_by_vendor = {} # { vendor_id => { total: , name: } }
        for li in self.line_items:
            for variant in li.variant.product.variants:
                vendor_id = variant.vendor.id
                vendor_name = variant.vendor.presentation_en
                variant_id = variant.id
                price = variant.price
                summary_by_vendor.setdefault(vendor_id, { "total": 0, "name": vendor_name})
                summary_by_vendor[vendor_id]["total"] += (price * li.qty)
        return summary_by_vendor

    def refresh_item_total(self):
        try:
            self.item_total = sum([li.qty for li in self.line_items ])
            db.session.add(self)
            db.session.commit()
        except Exception, e:
            raise e


auto_userstamp(Ord)
auto_timestamp(Ord)


from datetime import datetime
@event.listens_for(Ord, 'init')
def generate_number(target, args, kwargs):
    n = "R%s" % datetime.now().strftime("%Y%m%d%H%M%S%f")
    target.number = n
    return target


class LineItem(db.Model, TimestampMixin):
    id = db.Column(db.Integer, primary_key=True)
    variant_id = db.Column(db.Integer, db.ForeignKey('variant.id', ondelete='CASCADE'))
    ord_id = db.Column(db.Integer, db.ForeignKey('ord.id', ondelete='CASCADE'))
    qty = db.Column(db.Integer)
    price = db.Column(db.Numeric(scale=2))
    cost_price = db.Column(db.Numeric(scale=2))

    __table_args__ = ( db.UniqueConstraint('variant_id', 'ord_id', name='uniq_variant_ord_line_item'), )
    ord = db.relationship("Ord", backref=db.backref("line_items"))
    variant = db.relationship("Variant", backref=db.backref("line_items"))

    @property
    def total(self):
        return self.qty * self.price


auto_timestamp(LineItem)
def _refresh_order_item_total(mapper, conn, target):
    o = Ord.query.get(target.ord_id)
    ord_tbl = Ord.__table__
    conn.execute(ord_tbl.update().
                 where(ord_tbl.c.id==target.ord_id).
                 values(item_total=sum([li.qty for li in o.line_items ])))

event.listen(LineItem, 'after_delete', _refresh_order_item_total)
event.listen(LineItem, 'after_insert', _refresh_order_item_total)
event.listen(LineItem, 'after_update', _refresh_order_item_total)
