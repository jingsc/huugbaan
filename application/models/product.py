# coding: utf-8
import random
from ._util import sample_from_query
from ._base import db, TimestampMixin, UserstampMixin, \
                   auto_userstamp, auto_timestamp
from datetime import datetime
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy import not_
from searchable_mixin import SearchableMixin
import itertools


# class ProductTaxons(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     product_id = db.Column(db.Integer, nullable=False)
#     taxon_id = db.Column(db.Integer, nullable=False)
#     position = db.Column(db.Integer, nullable=False) # ??


class Product(db.Model, TimestampMixin, UserstampMixin, SearchableMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name_en = db.Column(db.String(250), unique=True)
    description_en = db.Column(db.Text)
    name_th = db.Column(db.String(250), unique=True)
    description_th = db.Column(db.Text)
    slug = db.Column(db.String(50), unique=True)
    product_code = db.Column(db.String(150), unique=True)
    brand_id = db.Column(db.Integer, db.ForeignKey('brand.id'))

    barcode = db.Column(db.String(100))
    shipping_dimension = db.Column(db.String(100))
    shipping_packing = db.Column(db.String(100))

    images = db.Column(JSONB)
    # json images structure
    # product.images[filename] = {
    #         'uri': // string uri of full-size image
    #         'thumb_uri':  // string uri of thumb size imgae 100x100
    #         'mid_thumb_uri': // string uri of mid thumb size image 300x300
    #         'created_at':  // datetime format
    #         'is_master': // boolean, there must be a master recrod,
    #     }

    properties = db.Column(JSONB)

    # available_on = db.Column(db.DateTime)
    # promotionable = db.Column(db.Boolean)

    # relationship
    scenes = association_proxy('scenes_products', 'scene')
    prototype_id = db.Column(db.Integer, db.ForeignKey('prototype.id'))
    styles = db.relationship('Style', 
                              secondary='product_style',
                              backref=db.backref('products'))


    # meta_description = db.Column(db.Text)
    # meta_keywoards = db.Column(db.String(100))
    # meta_title = db.Column(db.Unicode(100))

    # pricing
    # tax_category_id = db.Column(db.Integer)
    # shipping_category_id = db.Column(db.Integer)

    # methods --------------------------------------------------------
    @property
    def name(self):
        return self.name_en

    @property
    def description(self):
        return self.description_en

    @property
    def master_image(self):
        res = filter(lambda r: self.images[r]["is_master"], 
                     self.images.keys())
        if res:
            return self.images[res[0]]
        else:
            # make new master in case missing one
            master = self._make_master_image()
            return master

    def _make_master_image(self):
        new_master = self.images.keys() and self.images.keys()[0]
        if new_master:
            self.images[new_master]['is_master'] = True
            flag_modified(self, "images")
            db.session.add(self)
            db.session.commit()
        return new_master

    @property
    def sample_price(self):
        v = self.variants and self.variants[0]
        if v:
            return v.price
        return 0

    @property
    def related_products(self, n=6):
        return self.get_related_products(n)

    def get_related_products(self, n=6):
        qry = Product.query.filter_by(prototype_id=self.prototype_id).\
                             filter(Product.id != self.id)
        return sample_from_query(qry, n)

    def __repr__(self):
        return self.name_en
        # return '<Product %s>' % self.name_en

auto_userstamp(Product)
auto_timestamp(Product)
