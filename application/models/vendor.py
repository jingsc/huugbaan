# coding: utf-8
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, TimestampMixin, UserstampMixin, auto_userstamp, auto_timestamp
from sqlalchemy.dialects.postgresql import JSONB
from .address import HasAddressesMixin


class Vendor(db.Model, HasAddressesMixin, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name_en = db.Column(db.Unicode(50), unique=True)
    name_th = db.Column(db.Unicode(50), unique=True)
    presentation_en = db.Column(db.Unicode(200), unique=True)
    presentation_th = db.Column(db.Unicode(200), unique=True)

    # brand_owners = db.Column(JSONB)
    vendor_image = db.Column(db.String())
    # relationship
    products = db.relationship('Product', 
                                secondary='variant', 
                                backref=db.backref('vendors'))

    brand_owners = db.relationship('Brand', 
                             secondary='vendor_brand', 
                             backref=db.backref('vendors'))

    def get_main_address(self):
        a = filter(lambda a: a.is_main, self.addresses)
        return a[0] if len(a) else False
        

    def __repr__(self):
        return self.name_en
        return '<Vendor %s>' % self.name_en


auto_userstamp(Vendor)
auto_timestamp(Vendor)
