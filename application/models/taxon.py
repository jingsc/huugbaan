# coding: utf-8
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, TimestamMixin


class Taxon(db.Model, TimestampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name = db.Column(db.String(50), unique=True)
    presentation = db.Column(db.Unicode(200), nullable=False)

    def __repr__(self):
        return '<Property %s>' % self.name
