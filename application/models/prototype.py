# coding: utf-8
import json
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from ._base import db, TimestampMixin, UserstampMixin, \
                   auto_userstamp, auto_timestamp
from sqlalchemy.dialects.postgresql import JSONB


# class PrototypeTaxon(db.Model, TimestampMixin):
#     id = db.Column(db.Integer, primary_key=True)
#     prototype_id = db.Column(db.Integer, nullable=False)
#     taxon_id = db.Column(db.Integer, nullable=False)


class Prototype(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name = db.Column(db.String(50), unique=True, nullable=False)
    name_th = db.Column(db.String(150), unique=True)
    properties = db.Column(JSONB)
    optional_properties = db.Column(JSONB)
    choose_properties = db.Column(JSONB) # {'key1': ['v1', 'v2']}
    check_properties = db.Column(JSONB) # ['key1', 'key2']

    # relationship
    # properties = db.relationship("Property", 
    #                             secondary='property_prototype')
    products = db.relationship('Product', backref=db.backref('prototype'))
    disabled = db.Column(db.Boolean(), default=False)

    def create_default_props(self):
        d = dict()
        for e in self.properties:
            d.setdefault(e, '')
        for k in self.optional_properties.keys():
            d.setdefault(k, '')
        for k in self.choose_properties.keys():
            d.setdefault(k, '')
        for e in self.check_properties:
            d.setdefault(e, False)
        return d

    def __repr__(self):
        return '<Prototype %s>' % self.name


auto_userstamp(Prototype)
auto_timestamp(Prototype)
