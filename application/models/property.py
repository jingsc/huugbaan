# coding: utf-8
from datetime import datetime
from ._base import db, TimestampMixin


# class ProductProperty(db.Model, TimestampMixin):
#     id = db.Column(db.Integer, primary_key=True)
#     product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
#     property_id = db.Column(db.Integer, db.ForeignKey('property.id'))
#     value = db.Column(db.String)
#     position = db.Column(db.Integer)
# 
# 
# class PropertyPrototype(db.Model, TimestampMixin):
#     id = db.Column(db.Integer, primary_key=True)
#     prototype_id = db.Column(db.Integer, db.ForeignKey('prototype.id'))
#     property_id = db.Column(db.Integer, db.ForeignKey('property.id'))


class Property(db.Model, TimestampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name = db.Column(db.String(50), unique=True)
    presentation = db.Column(db.Unicode(200), nullable=False)

    # relationship
    def __repr__(self):
        return '<Property %s>' % self.name
