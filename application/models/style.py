# coding: utf-8
import sqlalchemy as sa
from ._base import db, TimestampMixin, UserstampMixin, \
                   auto_userstamp, auto_timestamp
from datetime import datetime
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm.attributes import flag_modified
from searchable_mixin import SearchableMixin
import itertools


class Style(db.Model, TimestampMixin, UserstampMixin):
    id = db.Column(db.Integer, primary_key=True)

    # general
    name = db.Column(db.String(250), unique=True, nullable=False)
    name_th = db.Column(db.String(250), unique=True)
    disabled = db.Column(db.Boolean(), default=False)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Style %s>' % self.name


auto_userstamp(Style)
auto_timestamp(Style)


class ProductStyle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    style_id = db.Column(db.Integer, db.ForeignKey("style.id"))
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"))
    # relationship
    __table_args__ = ( db.UniqueConstraint('product_id', 'style_id', name='uniq_product_style'), )
    product = db.relationship("Product", backref=db.backref("product_styles"))
    style = db.relationship("Style", backref=db.backref("product_styles"))


class SceneStyle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    style_id = db.Column(db.Integer, db.ForeignKey("style.id"))
    scene_id = db.Column(db.Integer, db.ForeignKey("scene.id"))
    # relationship
    __table_args__ = ( db.UniqueConstraint('scene_id', 'style_id', name='uniq_scene_style'), )
    scene = db.relationship("Scene", backref=db.backref("scene_styles"))
    style = db.relationship("Style", backref=db.backref("scene_styles"))
