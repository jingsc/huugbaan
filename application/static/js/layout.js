(function () {
    "use strict";

    // Add CSRF token header for Ajax request
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", g.csrfToken);
            }
        }
    });

    // Flash message
    setTimeout(showFlash, 200);
    setTimeout(hideFlash, 3000);

    /**
     * Show flash message.
     */
    function showFlash() {
        $('.flash-message').slideDown('fast');
    }

    /**
     * Show flash message with a message
     */
    function showFlashMsg(msg){
        $('.flash-message').html(msg).slideDown('fast');
    }

    /**
     * Hide flash message.
     */
    function hideFlash() {
        $('.flash-message').slideUp('fast');
    }

    /**
     * Util Multiline String
     */
    window.mstring = function(fn){
        var reg = /\/\*([\s\S]*)\*\//im;
        if(typeof fn == 'function'){
            return reg.exec(fn.toString())[1];
        }
        return false;
    }

    /**
     * Show hide sub menu
     */
    $(document).ready(function(){
        $('a.username').hover(function(){
            $('div.username-submenu').show(); 
        });
        $('div.username-submenu').mouseleave(function(){
            $(this).hide();
        }); 
    });
})();
