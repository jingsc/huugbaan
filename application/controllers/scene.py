# coding: utf-8
from flask import render_template, Blueprint, redirect, request, abort
from flask_login import login_required, logout_user, current_user
from sqlalchemy import func
from application.utils.pagination import Pagination
from ..models import (SceneType, Style, SceneStyle,
                      Scene, Product, db, Ideabook)

bp = Blueprint('scene_controller', __name__)

PER_PAGE = 30

@bp.route('/scenes/', defaults={'page': 1})
@bp.route('/scenes/page/<int:page>')
def index(page):
    offset = (page - 1) * PER_PAGE
    qry = Scene.query

    scene_type_id = request.args.get("scene_type_id", None)
    filter_type = None
    if scene_type_id:
        scene_type_id = int(scene_type_id) 
        filter_type = SceneType.query.get(scene_type_id)
        qry = qry.filter_by(scene_type_id=scene_type_id)

    scene_style_id = request.args.get("scene_style_id", None)
    filter_style = None
    if scene_style_id:
        scene_style_id = int(scene_style_id) 
        filter_style = Style.query.get(scene_style_id)
        qry = qry.filter(Scene.styles.any(Style.id==scene_style_id))

    scenes = qry.limit(PER_PAGE).offset(offset).all()

    if not scenes and page != 1:
        abort(404)

    total = qry.count() #db.session.query(func.count("*")).select_from(Scene).scalar()
    pagination = Pagination(page, PER_PAGE, total)
    all_style_id = [ss.style_id for ss in SceneStyle.query.distinct(SceneStyle.style_id)]
    styles = Style.query.filter(Style.id.in_(all_style_id))
    types = SceneType.query.all()

    return render_template('scene/index.html',
                            types=types,
                            filter_type_id=scene_type_id,
                            filter_style_id=scene_style_id,
                            filter_type=filter_type,
                            filter_style=filter_style,
                            styles=styles,
                            scenes=scenes,
                            paginator=pagination,
                            total=total)


@bp.route('/scene/<int:sid>')
def show(sid):
    s = Scene.query.get(sid)
    if s:
        if current_user.is_authenticated:
            ideabooks = Ideabook.query.filter_by(user_id=current_user.id).all()
        else:
            ideabooks = []
        return render_template('scene/show.html', 
                                scene=s,
                                ideabooks=ideabooks)
    else:
        flash('Scene not found!')
        return redirect('scene_controller.index')
