# coding: utf-8
from flask import render_template, Blueprint, redirect, url_for, jsonify, request, flash, abort
from flask_login import login_required, logout_user, \
                        current_user, login_user
from ..forms import SigninForm, SignupForm
from ..models import db, Scene, User, UserScene, \
                     Ord, LineItem, Variant, \
                     Ideabook, IdeabookPhoto
from ..utils import filters

bp = Blueprint('ideabook', __name__, url_prefix='/ideabook')


@bp.route('/', methods=['GET'])
@login_required
def list():
    return render_template('ideabook/list_ideabook.html')


@bp.route("/show/<int:ideabook_id>", methods=["GET"])
@login_required
def show(ideabook_id):
    # check whethe ideabook_id existing and corresponding with user
    ib = Ideabook.query.filter_by(user_id=current_user.id, id=ideabook_id).first()
    if ib:
        return render_template("ideabook/show.html", ideabook=ib)
    else:
        # not found resource
        abort(404)


@bp.route('/', methods=['POST'])
@login_required
def create():
    # accept only ajax
    if request.is_xhr:
        try:
            name = request.json.get('name', None)
            if name:
                ib = current_user.create_ideabook(name)
                return jsonify(success=True, record=ib.attrs())
            return jsonify(success=False, message="Ideabook name should not be blank.")
        except Exception, e:
            return jsonify(success=False, message=e.message)
    else:
        return jsonify(success=False, message='Invalid request.')


@bp.route('/remove_item/<int:line_item_id>', methods=['DELETE'])
@login_required
def remove_item(line_item_id):
    if request.is_xhr:
        try:
            li = LineItem.query.get(line_item_id)
            db.session.delete(li)
            db.session.commit();
            return jsonify(success=True, 
                    result={'summary_by_vendor': _get_summary_total_for_cart_view(), 
                            'item_total': current_user.cart_order.item_total },
                           message='Remove the item successful.')
        except Exception, e:
            return jsonify(success=False, message=e.message)
    return jsonify(success=False, message='Invalid request.')


@bp.route('/add_photo', methods=['POST'])
@login_required
def add_photo():
    new_ideabook_name = request.form.get('new_ideabook_name')
    ideabook_id = request.form.get('ideabook_id')
    photo_id = request.form.get('photo_id')
    photo_source = request.form.get('photo_source') # product or scene
    notes = request.form.get('notes')
    try:
        if new_ideabook_name:
            # create new ideabook
            ib = Ideabook(name=new_ideabook_name, 
                          user_id=current_user.id)
            db.session.add(ib)
            db.session.commit()
        else:
            # find existing
            ib = Ideabook.query.get(ideabook_id)

        # check if already added
        ip = IdeabookPhoto.query.filter_by(ideabook_id=ib.id, 
                photo_id=photo_id, assoc_type=photo_source).first()
        if ip:
            flash("This photo has already been added to the ideabook.", "error")
            return redirect(request.referrer)

        ip = IdeabookPhoto(ideabook_id=ib.id,
                           photo_id=photo_id, 
                           assoc_type=photo_source, 
                           notes=notes)
        # save ideabook-photo
        db.session.add(ip)
        db.session.commit()
        flash("Photo has been added to the ideabook.")
        return redirect(request.referrer)
    except Exception, e:
        flash(e.message)
        return redirect(request.referrer)
