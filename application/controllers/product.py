# coding: utf-8
from flask import render_template, Blueprint, redirect, request, url_for, flash, session
from flask_login import login_required, logout_user, current_user
from sqlalchemy import or_, func
from application.utils.pagination import Pagination
from ..models import (SceneType, Scene, Product, 
                      Style, ProductStyle,
                      Prototype, Ideabook, db, 
                      Variant, Prototype)

bp = Blueprint('product_controller', __name__)

PER_PAGE = 10

@bp.route('/products', defaults={'page': 1})
@bp.route('/products/page/<int:page>')
def index(page):
    offset = (page - 1) * PER_PAGE
    qry = Product.query

    # keep filter
    session.setdefault('already_joined_varaint', False)
    session.setdefault('filter_prototype_id', None)
    session.setdefault('fitler_product_style_id', None)
    session.setdefault('filter_from_price', 0)
    session.setdefault('filter_to_price', None)

    if request.args.get('reset_filter_prototype_id') and session.has_key('filter_prototype_id'):
        session.pop('filter_prototype_id')

    if request.args.get('reset_filter_product_style') and session.has_key('filter_product_style_id'):
        session.pop('filter_product_style_id')

    if request.args.get('reset_filter_from_to_price'):
        session['already_joined_varaint'] = False
        if session.has_key('filter_from_price'):
            session.pop('filter_from_price')

        if session.has_key('filter_to_price'):
            session.pop('filter_to_price')
        

    prototype_id = request.args.get("prototype_id", None) or session.get('filter_prototype_id')
    if prototype_id:
        prototype_id = int(prototype_id) 
        session['filter_prototype_id'] = prototype_id
        qry = qry.filter_by(prototype_id=prototype_id)

    product_style_id = request.args.get("product_style_id", None) or session.get('filter_product_style_id')
    if product_style_id:
        product_style_id = int(product_style_id) 
        session['filter_product_style_id'] = product_style_id
        qry = qry.filter(Product.styles.any(Style.id==product_style_id))

    from_price = request.args.get("from_price", None) or session.get('filter_from_price')
    if from_price:
        from_price = float(from_price) 
        session['filter_from_price'] = from_price
        qry = qry.join(Variant).filter(Variant.price >= from_price)
        session['already_joined_varaint'] = True

    to_price = request.args.get("to_price", None) or session.get('filter_to_price')
    if to_price:
        to_price = float(to_price) 
        session['filter_to_price'] = to_price
        if session['already_joined_varaint']:
            qry = qry.filter(Variant.price <= to_price)
        else:
            qry = qry.join(Variant).filter(Variant.price <= to_price)
            session['already_joined_varaint'] = True

    products = qry.limit(PER_PAGE).offset(offset).all()
    if not products and page != 1:
        abort(404)

    total = qry.count() # db.session.query(func.count("*")).select_from(Product).scalar()
    paginator = Pagination(page, PER_PAGE, total)

    all_style_id = [ps.style_id for ps in ProductStyle.query.distinct(ProductStyle.style_id)]
    product_styles = Style.query.filter(Style.id.in_(all_style_id))

    # ref filters
    prototypes = Prototype.query.filter_by(disabled=False).all()

    return render_template('product/index.html',
                            products=products,
                            prototypes=prototypes,
                            paginator=paginator,
                            product_styles=product_styles,
                            page=page,
                            offset=offset,
                            per_page=PER_PAGE,
                            total=total)


@bp.route('/products/<int:product_id>')
def show(product_id):
    p = Product.query.get(product_id)
    if p:
        if current_user.is_authenticated:
            ideabooks = Ideabook.query.filter_by(user_id=current_user.id).all()
        else:
            ideabooks = []

        _related_scenes_name = []
        related_scenes = []
        for rp in p.related_products:
            for s in rp.scenes:
                if s.name not in _related_scenes_name:
                    _related_scenes_name.append(s.name)
                    related_scenes.append(s)

        return render_template('product/show.html', 
                                product=p, 
                                related_scenes=related_scenes,
                                ideabooks=ideabooks)
    else:
        flash("Product was not found!", 'error')
    return redirect(url_for('product_controller.index'))
