# coding: utf-8
from flask import ( render_template, 
                    Blueprint, 
                    redirect, 
                    url_for, 
                    abort,
                    flash, 
                    request )
import sqlalchemy as sa
from sqlalchemy.sql import exists
from flask_login import ( login_required, 
                          logout_user,
                          current_user, 
                          login_user )
from ..forms import SigninForm, SignupForm
from ..models import db, Scene, User, Product, SceneType

bp = Blueprint('site', __name__)

SHOW_NUMBER_OF_SCENES = 5
SHOW_NUMBER_OF_SCENE_TYPES = 3
import random
@bp.route('/')
def index():
    signin_form = SigninForm()
    signup_form = SignupForm()

    # TODO faster
    s = Scene.query.all()
    if len(s) > SHOW_NUMBER_OF_SCENES:
        s = random.sample(s, SHOW_NUMBER_OF_SCENES)
    random.shuffle(s)

    st = SceneType.query.filter_by(disabled=False).filter(exists().where(Scene.id==SceneType.id)).all()
    if len(st) > SHOW_NUMBER_OF_SCENE_TYPES:
        st = random.sample(st, SHOW_NUMBER_OF_SCENE_TYPES)
    random.shuffle(st)

    return render_template('site/index/index.html', 
                            scenes=s,
                            show_types=st,
                            signin_form=signin_form,
                            signup_form=signup_form)


@bp.route('/about')
def about():
    """About page."""
    return render_template('site/about/about.html')


@bp.route('/login', methods=['GET', 'POST'])
def login():
    signin_form = SigninForm()
    signup_form = SignupForm()

    submit = request.form.get('submit', None)

    if submit == 'sign_in':
        if signin_form.validate_on_submit():
            login_user(signin_form.user)

    if submit == 'sign_up':
        if signup_form.validate_on_submit():
            params = signup_form.data.copy()
            params.pop('repassword')
            user = User(**params)
            user.username = user.email
            user.active = True
            db.session.add(user)
            db.session.commit()
            login_user(user)

    if current_user.is_active:
        flash('Welcome %s' % current_user.name)
        return redirect('/')
   
    return render_template('site/login/login.html', 
                            signup_form=signup_form, 
                            signin_form=signin_form)


@bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        params = form.data.copy()
        params.pop('repassword')
        user = User(**params)
        user.username = user.email
        user.active = True
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('site.index'))
    return render_template('site/login/signup.html', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect('/')


@login_required
@bp.route('/done')
def done():
    return render_template('site/done/done.html')


PER_PAGE = 30

@bp.route("/search")
def search():
    term = request.args.get('search', None)
    if term:
        lterm = "%%%s%%" % term
        products = Product.query.filter(sa.or_(Product.name_en.like(lterm), 
                                               Product.name_th.like(lterm))).limit(PER_PAGE).offset(0).all()
        scenes = Scene.query.filter(Scene.name.like(lterm)).limit(PER_PAGE).offset(0).all()
    else:
        products = [] # Product.query.limit(PER_PAGE).offset(0).all()
        scenes = [] # Scene.query.limit(PER_PAGE).offset(0).all()
    return render_template("site/search/search.html", 
                            search=term,
                            scenes=scenes, 
                            products=products)
