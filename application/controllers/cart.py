# coding: utf-8
from flask import render_template, Blueprint, redirect, url_for, jsonify, request
from flask_login import login_required, logout_user, \
                        current_user, login_user
from ..forms import SigninForm, SignupForm
from ..models import db, Scene, User, UserScene, \
                     Ord, LineItem, Variant
from ..utils import filters

bp = Blueprint('cart', __name__, url_prefix='/cart')


def _get_summary_total_for_cart_view():
    cart = current_user.cart_order
    summary_by_vendor = cart.get_total_by_vendor()
    for vendor_id in summary_by_vendor:
        total = summary_by_vendor[vendor_id]['total']
        summary_by_vendor[vendor_id]['total'] = filters.baht(total)
    return summary_by_vendor


@bp.route('/<int:cart_id>', methods=['PUT'])
@login_required
def add_to_cart(cart_id):
    # accept only ajax
    if request.is_xhr:
        try:
            line_item_id = request.json.get('line_item_id')
            li = LineItem.query.get(line_item_id)
            qty = request.json.get('qty')
            if qty: li.qty = qty
            db.session.add(li)
            db.session.commit()
        except Exception, e:
            return jsonify(success=False, message=e.message)

        summary_by_variant = {} # id => total
        for variant in li.variant.product.variants:
            summary_by_variant.setdefault(variant.id, { 'total': filters.baht(li.qty * variant.price) })

        res = { 'summary_by_variant': summary_by_variant, 
                 'summary_by_vendor': _get_summary_total_for_cart_view(),
                 'item_total': current_user.cart_order.item_total }
        return jsonify(message='success', success=True, result=res)
    else:
        return jsonify(success=False, message='Invalid request.')


@bp.route('/remove_item/<int:line_item_id>', methods=['DELETE'])
@login_required
def remove_item(line_item_id):
    if request.is_xhr:
        try:
            li = LineItem.query.get(line_item_id)
            db.session.delete(li)
            db.session.commit();
            return jsonify(success=True, 
                    result={'summary_by_vendor': _get_summary_total_for_cart_view(), 
                            'item_total': current_user.cart_order.item_total },
                           message='Remove the item successful.')
        except Exception, e:
            return jsonify(success=False, message=e.message)
    return jsonify(success=False, message='Invalid request.')
