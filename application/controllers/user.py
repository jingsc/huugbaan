# coding: utf-8
from flask import render_template, Blueprint, redirect, url_for, jsonify, request, flash
from flask_login import login_required, logout_user, \
                        current_user, login_user
from ..forms import SigninForm, SignupForm, UserProfile
from ..models import db, Scene, User, UserScene, \
                     Ord, LineItem, Variant

bp = Blueprint('user_controller', __name__, url_prefix='/user')


@bp.route('/add_to_ideabook', methods=['POST'])
@login_required
def add_to_ideabook():
    # accept only ajax
    if request.is_xhr:
        scene_id = request.json.get('scene_id')
        us = UserScene(user_id=current_user.id, scene_id=scene_id)
        try:
            db.session.add(us)
            db.session.commit()
        except Exception, e:
            return jsonify(success=False, message=e.message)

        return jsonify(message='success', success=True)
    else:
        return jsonify(success=False, message='Invalid request.')


@login_required
@bp.route('/remove_from_ideabook', methods=['POST'])
def remove_from_ideabook():
    # accept only ajax
    if request.is_xhr:
        scene_id = request.json.get('scene_id')
        us = filter(lambda r: r.scene_id == scene_id, current_user.user_scenes)
        if bool(us): 
            us = us[0]
            try:
                db.session.delete(us)
                db.session.commit()
            except Exception, e:
                return jsonify(success=False, message=e.message)
        return jsonify(message='The scene has already be removed', success=True)
    else:
        return jsonify(success=False, message='Invalid request.')


@login_required
@bp.route('/add_to_cart', methods=['POST'])
def add_to_cart():
    if request.is_xhr:
        product_id = request.json.get('product_id')
        qty = request.json.get('qty') or 1
        if product_id is None:
            return jsonify(message='Not found product!', sucess=False)

        # 1. find or create user's order with cart state
        # 2. add create LineItem with product_id and link to Order
        o = current_user.cart_order

        if o is None:
            # create Ord with "cart" state
            o = Ord(state="cart", user_id=current_user.id,
                    item_total=qty, item_count=1)
            db.session.add(o)
            db.session.commit()

        # add master varinat of selected product into the ord
        variant = Variant.query.filter_by(product_id=product_id, 
                                          is_master=True).first()
        if variant:
            li = LineItem.query.filter_by(variant_id=variant.id, 
                                          ord_id=o.id).first()
            if li is None: # create new LineItem for the first adding
                li = LineItem(variant_id=variant.id, ord_id=o.id,
                              price=variant.price, qty=qty,
                              cost_price=variant.price)

            else: # increase qty if already added
                li.qty = li.qty + qty
        else:
            return jsonify(message='Not found variant!', sucess=False)

        db.session.add(li)
        db.session.commit()
        return jsonify(message='success', success=True, item_total=o.item_total)
    else:
        return jsonify(success=False, message='Invalid Request.')


@login_required
@bp.route('/view_cart', methods=['GET'])
def view_cart():
    cart = current_user.cart_order
    summary_by_vendor = {} # { vendor_id => [variant_id, ..] }
    vendors = []
    for li in cart.line_items:
        for variant in li.variant.product.variants:
            vendor_id = variant.vendor.id
            if variant.vendor not in vendors:
                vendors.append(variant.vendor)
            vendor_name = variant.vendor.presentation_en
            variant_id = variant.id
            price = variant.price
            summary_by_vendor.setdefault(vendor_id, { "total": 0, "name": vendor_name})
            summary_by_vendor[vendor_id]["total"] += (price * li.qty)

    return render_template('user/view_cart.html', cart=cart, summary_by_vendor=summary_by_vendor, vendors=vendors)


@login_required
@bp.route("/edit", methods=["GET", "POST"])
def edit():
    form = UserProfile()
    if request.method == "POST":
        if form.validate_on_submit():
            form.populate_obj(current_user)
            db.session.add(current_user)
            db.session.commit()
            flash("User profile has been updated.")
    else: 
        form = UserProfile(obj=current_user)
    return render_template('user/edit.html', user=current_user, form=form)
