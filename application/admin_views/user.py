# coding: utf-8
import os
import flask_admin as fa
import os.path as op
from flask import request, url_for
from flask_admin.form import rules
from flask_admin.contrib.sqla import ModelView
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from .util import display_timestamp_format, list_thumbnail, image_prefix_date_namegen
from flask_login import current_user
from wtforms import fields, widgets
from ..models import db, User

avatar_path = op.join(op.dirname(__file__), '../static/avatar_images')
try:
    os.mkdir(avatar_path)
except OSError:
    pass


class DisabledTextFieldWhenEdit(fields.TextField):
    def __call__(self, *args, **kwargs):
        if request.endpoint == 'user.edit_view':
            kwargs.setdefault('readonly', True)
        return super(DisabledTextFieldWhenEdit, self).__call__(*args, **kwargs)


class UserView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_admin

    form_excluded_columns = ['created_at', 'updated_at', 'social_auth', 'user_scenes', 'created_by', 'updated_by']
    column_list = ['avatar', 'name', 'username', 'email', 'active', 'is_admin', 
                   'created_at', 'created_by', 'updated_at', 'updated_by']

    form_create_rules = ('name', 'username', 'password', 'retype_password', 'email', 'avatar', 'is_admin' )
    form_edit_rules =  ('name', 'username', 'email', 'avatar', 'is_admin' )
    
    # validate rules
    def recheck_password(form, field):
        if field.data != form.retype_password.data:
            raise(ValidationError("Password is not matched"))

    def uniq_username(form, field):
        if request.endpoint == 'user.create_view':
            u = User.query.filter_by(username=field.data).first()
        else:
            id = request.args.get('id')
            u = User.query.filter_by(username=field.data).filter(User.id != id).first()

        if u: raise(ValidationError("Username should be uniqness."))

    form_args = dict(
        username=dict(validators=[DataRequired(), uniq_username]),
        email=dict(validators=[Email()]),
        password=dict(validators=[recheck_password]),)
    form_overrides = dict(password=fields.PasswordField, 
                          username=DisabledTextFieldWhenEdit,
                          email=DisabledTextFieldWhenEdit)
    form_extra_fields = dict(
            retype_password=fields.PasswordField('Retype Password'),
            avatar=fa.form.ImageUploadField("Avatar",
                                            base_path=avatar_path,
                                            namegen=image_prefix_date_namegen('avatar'),
                                            url_relative_path='avatar_images/',
                                            thumbnail_size=(100, 100, True)))

    column_formatters = {
        'created_at': display_timestamp_format,
        'updated_at': display_timestamp_format,
        'avatar': list_thumbnail("avatar", "avatar_images", 
                                 default_url="/static/image/default.jpg"),
    }

    def delete_model(self, model):
        model.active = False
        db.session.add(model)
        db.session.commit()
        return True
