# coding: utf-8
import os
import jinja2
import os.path as op
import flask_admin as fa
from flask_admin.model.ajax import AjaxModelLoader, DEFAULT_PAGE_SIZE
from flask_admin.form import Select2Widget
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.ajax import QueryAjaxModelLoader
from flask_admin.helpers import (get_form_data, validate_form_on_submit,
                                 get_redirect_target, flash_errors)
from wtforms import Field
from wtforms.widgets import HTMLString
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from .address import AddressInlineForm
from .prototype import TagListField
from .util import display_timestamp_format, image_prefix_date_namegen, list_thumbnail
from ..models import Address, db, Brand


logo_path = op.join(op.dirname(__file__), '../static/vendor_images')
try:
    os.mkdir(logo_path)
except OSError:
    pass


class VendorView(ModelView):
    # can_view_details = True
    form_excluded_columns = ['created_at', 'updated_at', 'products']
    column_list = ('vendor_image', 'name_en', 'name_th', 'brand_owners', 
                   'presentation_en', 'main_contact',
                   'created_at', 'created_by', 
                   'updated_at', 'updated_by', )
    form_create_rules = ('name_en', 
                         'name_th', 
                         'presentation_en', 
                         'presentation_th', 
                         'vendor_image',
                         'brand_owners',
                         'addresses',)
    form_edit_rules = form_create_rules
    edit_template = 'admin/model/with_inline_address.html'
    column_searchable_list = ('name_en', )
    column_labels = dict(vendor_image='Logo')
    inline_models = (AddressInlineForm(),)
    form_args = dict(
        name_en=dict(validators=[DataRequired()]),
        presentation_en=dict(validators=[DataRequired()]),
        brand_owners=dict(query_factory=lambda: Brand.query.filter_by(disabled=False).all()),
    )
    form_extra_fields = dict(
        vendor_image=fa.form.ImageUploadField("Logo",
                                        base_path=logo_path,
                                        namegen=image_prefix_date_namegen('vendor-logo'),
                                        url_relative_path='vendor_images/',
                                        thumbnail_size=(100, 100, True)))

    def _display_main_address(view, context, model, name):
        main = model.get_main_address() 
        if main:
            return "Tel:{phone}\nEmail:{email}".format(phone=main.phone, email=main.email)
        return 'There is no main contact.'

    column_formatters = {
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            'main_contact': _display_main_address,
            'vendor_image': list_thumbnail("vendor_image", "vendor_images"),
        }
