# coding: utf-8
import os
import os.path as op
import json
from datetime import datetime
from copy import copy

import jinja2
import wtforms.fields as fd
import wtforms.validators as vt
from flask_admin.form import rules
from flask_admin import expose
from flask import request, redirect, jsonify, flash
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.ajax import create_ajax_loader, QueryAjaxModelLoader
from flask_admin import Admin, form
from flask_admin.form.widgets import Select2TagsWidget
from flask_admin.form.fields import Select2TagsField
from flask_admin.model.fields import AjaxSelectMultipleField
from flask_admin.babel import gettext
from flask import url_for
from wtforms import ( TextField, DateField,
                      fields, widgets,
                      StringField, TextAreaField)
from wtforms.fields import SelectField, Field
from wtforms.widgets import HTMLString
from wtforms.validators import ValidationError, DataRequired
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from flask_wtf import Form

from flask.blueprints import Blueprint
from jinja2 import Markup

from ..models import ( Scene, SceneProduct, 
                       db, Product, SceneType, 
                       Style )
from .util import display_timestamp_format

try:
    # use for image size validator
    from PIL import Image, ImageOps
except ImportError:
    Image = None
    ImageOps = None


# store images folder
# init static folder for scene images
folder_name = 'scene_images'
file_path = op.join(op.dirname(__file__), ('../%s' % folder_name))
bp_scene_view = Blueprint('scene_view',  __name__,
                static_url_path='/scene_images', 
                static_folder=file_path)
MIN_IMAGE_SIZE = ( 500, 166 )


from random import random
class GeolocationWidget(object):
    def __call__(self, field, **kwargs):
        template = '''
        <div class='input-group'>
            <div class='input-group-btn'>
                <button class="btn btn-default btn-use-current-location-for-{{ id_ }}" type="button">Use Current Location</button>
            </div>
            <input id='{{ id_ }}' class="form-control" type='text' name='{{ field.name }}' value='{{ field.data or "" }}'>
        </div>

        <script>
        (function(){
            // wait jquery
            setTimeout(function(){
                $('.btn-use-current-location-for-{{ id_ }}').click(function(){
                    var locationField = $(this).parent().next();
                    locationField.val("waiting for access current location ...");
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(pos){
                            locationField.val(pos.coords.latitude + "," + pos.coords.longitude);
                        });
                    } else { 
                        locationField.val("Geolocation is not supported by this browser.");
                    }
                });

            },100); 
        })();
        </script>
        '''

        # use incase inline form
        random_id = "id%s" % (str(int(random() * 1000000000)))
        return HTMLString( jinja2.Template(template).render(field=field, id_=random_id) )


class GeolocationField(TextField):
    widget = GeolocationWidget()


def photo_resolution_validator(width, field):
    try:
        image = Image.open(field.data)
        if image.size[0] < MIN_IMAGE_SIZE[0] or image.size[1] < MIN_IMAGE_SIZE[1]:
            raise ValidationError("Scene photo should have minimum size at %sx%s." % MIN_IMAGE_SIZE)
    except: 
        # file may not be uploaded
        pass


def scene_name_gen():
    now = datetime.now().strftime('%Y%m%d_%H%M%S') # year month day hour min sec microsec
    return "photo_%s" % now


class SceneForm(Form):

    scene_type = QuerySelectField("Scene Type", allow_blank=False,
                                                query_factory=lambda : SceneType.query.filter_by(disabled=False).all(),
                                                get_pk=lambda r: r.id,
                                                get_label=lambda r: r.name,
                                                validators=[vt.DataRequired()])

    name = fd.StringField("Name", validators=[vt.DataRequired(), vt.Length(max=250)], default=scene_name_gen)
    name_th = fd.StringField("Name(TH)", validators=[vt.DataRequired(), vt.Length(max=250)], default=scene_name_gen)
    size = fd.SelectField("Size", choices=[('S', 'S'), ('M', 'M'), ('L', 'L'), ('XL', 'XL')])
    budget_type = fd.SelectField("Budget", choices=[('1', '$'), ('2', '$$'), ('3', '$$$')])
    description = fd.TextAreaField("Description")

    # loader = QueryAjaxModelLoader("name", db.session, SceneStyle, 
    #                               fields=(SceneStyle.name,))
    # styles = AjaxSelectMultipleField(loader, label="Styles")

    styles = QuerySelectMultipleField("Styles", allow_blank=True,
                                                query_factory=lambda : Style.query.filter_by(disabled=False).all(),
                                                get_pk=lambda r: r.id,
                                                get_label=lambda r: r.name)
    location = GeolocationField("Geolocation")
    location_text = fd.StringField("Location")
    owner = fd.StringField("Owner")
    designer = fd.StringField("Designer")
    photo_by = fd.StringField("Photo By")

    uri = form.ImageUploadField('Scene Photo',
          base_path=file_path,
          endpoint='scene_view.static',
          validators=[photo_resolution_validator],
          thumbnail_size=(100, 100, True))


class SceneView(ModelView):
    create_template = 'admin/scene_create.html'

    form_excluded_columns = ['created_at', 'updated_at']
    column_filters = ['name', 'name_th']
    column_list = ('uri', 
                   'scene_type', 
                   'styles',
                   'name', 
                   'name_th',
                   'description', 
                   'updated_at', )

    def _list_thumbnail(view, context, model, name):
        if not model.uri:
            return ''
        return Markup('<img src="%s">' % url_for('scene_view.static',
            filename=form.thumbgen_filename(model.uri)))

    size_choices = [('S', 'S'), ('M', 'M'), ('L', 'L'), ('XL', 'XL')]
    budget_choices = [('1', '$'), ('2', '$$'), ('3', '$$$')]

    form_args = dict( scene_type=dict(query_factory=lambda: SceneType.query.filter_by(disabled=False).all(),validators=[DataRequired()]),
                      styles=dict(query_factory=lambda: Style.query.filter_by(disabled=False).all(), validators=[DataRequired()]),
                      name_th=dict(validators=[DataRequired()]),
                    )

    form_overrides = dict(location=GeolocationField)

    column_formatters = {
            'uri': _list_thumbnail,
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    form_extra_fields = {
        'uri': form.ImageUploadField('Scene',
               base_path=file_path,
               endpoint='scene_view.static',
               validators=[photo_resolution_validator],
               thumbnail_size=(100, 100, True)),
        'size': SelectField('Size', choices=size_choices),
        'budget_type': SelectField('Budget', choices=budget_choices),
        'tags_data': fd.HiddenField("")
    }

    form_widget_args = dict(tags_data=dict(input_type='hidden'))

    @expose("/new/", methods=("GET", "POST"))
    def create_view(self):
        return_url = self.get_url('.index_view')

        if not self.can_create:
            return redirect(return_url)

        form = SceneForm()

        if self.validate_form(form):
            # update or create scene product
            try:
                scene = Scene()
                form.populate_obj(scene)
                db.session.add(scene)
                db.session.commit()
                scenes_products = json.loads(request.form.get('scenes_products'))

                for sp in scenes_products['data']:
                    _sp = SceneProduct()
                    _sp.product_id = sp.get('product_id')
                    _sp.scene_id = scene.id
                    _sp.pos_x = int(sp.get('x') * 10000)
                    _sp.pos_y = int(sp.get('y') * 10000)
                    db.session.add(_sp)
                    db.session.commit()

                flash(gettext('Record was successfully created.'), 'success')
                return redirect(return_url)
            except Exception, e:
                db.session.rollback()
                flash(gettext(e.message), 'error')
                return redirect(request.url)

        products = Product.query.all()
        _products = [ p.attrs() for p in products]
        return self.render("admin/scene_create.html",
                           form=form,
                           min_image_size=MIN_IMAGE_SIZE,
                           products=products,
                           _products=_products,
                           return_url=return_url)


    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        _id = request.args['id']
        if _id: scene = Scene.query.get(_id)
        else: abort(400)

        form = SceneForm(obj=scene)
        print form.validate_on_submit()

        if request.method == 'POST' and form.validate_on_submit():
            form.populate_obj(scene)
            db.session.add(scene)

            scenes_products = json.loads(request.form.get('scenes_products'))

            # remove the others not sent via form
            sp_ids = [ _sp.get('id') for _sp in scenes_products['data'] if _sp.get('id') ]
            for sp in scene.scenes_products:
                if sp.id not in sp_ids:
                    db.session.delete(sp)

            # update or create scene product
            for sp in scenes_products['data']:
                if sp.get('id'):
                    # update exist sp
                    _sp = SceneProduct.query.get(sp.get('id'))
                else:
                    _sp = SceneProduct()

                _sp.product_id = sp.get('product_id')
                _sp.scene_id = scene.id
                _sp.pos_x = int(sp.get('x') * 10000)
                _sp.pos_y = int(sp.get('y') * 10000)
                db.session.add(_sp)

            db.session.commit()
            flash('%s update complete!' % scene.name)
            return redirect(self.get_url('.index_view'))
        else: # request.method == 'GET':
            products = Product.query.all()
            products_in_the_scene = [ sp.product_id for sp in scene.scenes_products ]
            _products = [ p.attrs() for p in products]
            st = [ s.attrs() for s in SceneType.query.filter_by(disabled=False).all() ]
            styles = [ s.attrs() for s in Style.query.filter_by(disabled=False).all() ]
            stid = [ s.id for s in SceneType.query.all() ]
            return self.render("admin/scene_edit.html", 
                                model=scene,
                                products=products,
                                _products=_products,
                                scene_types=st,
                                styles=styles,
                                scene_types_ids=stid,
                                size_choices=self.size_choices,
                                budget_choices=self.budget_choices,
                                products_in_the_scene=products_in_the_scene,
                                form=form,
                                return_url=self.get_url('.index_view'))
