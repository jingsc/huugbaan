# coding: utf-8
from flask_admin.contrib.sqla import ModelView
from wtforms.validators import DataRequired, NumberRange, ValidationError
from .util import display_timestamp_format
from ..models import db


class SceneStyleView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at', 'scenes', 'scene_theme', 'created_by', 'updated_by']
    column_list = ('name', 'created_at', 'created_by', 'updated_at', 'updated_by', 'disabled')

    form_args = dict(
        name=dict(validators=[DataRequired()]),
    )

    column_formatters = {
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    def delete_model(self, model):
        model.disabled = True
        db.session.add(model)
        db.session.commit()
        return True
