# coding: utf-8
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin, form, expose
from flask import url_for, request, redirect, flash
from ..models import db, Prototype
from wtforms import Field, SelectField, TextField, TextAreaField
from wtforms.widgets import TextInput, html_params, HTMLString, HiddenInput, TextArea
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from .util import display_timestamp_format
from ._base import AdminModelView
import json


class PropertiesInput(TextInput):
    def __call__(self, field, **kwargs):
        kwargs['placeholder'] = 'example: value1, value2, value3'
        return super(PropertiesInput, self).__call__(field, **kwargs)


class TagListField(Field):
    widget = PropertiesInput()
    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [x.strip() for x in valuelist[0].split(',') if x.strip() ]
        else:
            self.data = []


from wtforms.compat import text_type, iteritems
class JSONWidget(object):
    def _build_json_kv_comp(self, k, v):
        html = []
        k = k or ''
        html.append(HTMLString("<div %s>" % html_params(class_='row kv-wrapper')))
        html.append(HTMLString("<div %s>" % html_params(class_='col-md-2')))
        html.append(HTMLString("<input %s>" % html_params(value=k, 
                                                          type='text', 
                                                          class_='json-key-input form-control',
                                                          placeholder='key')))
        html.append(HTMLString("</div>"))
        html.append(HTMLString("<div %s>" % html_params(class_='col-md-5')))

        v = v or []
        vl = ', '.join(v)
        html.append(HTMLString("<input %s>" % html_params(value=vl, 
                                                          type='text', 
                                                          class_='json-value-input form-control',
                                                          placeholder='example: value_1, value_2, value_3')))

        html.append(HTMLString("</div>"))
        # remove btn
        html.append(HTMLString("<div %s>" % html_params(class_='col-md-1')))
        if not k and not v:
            html.append(HTMLString("<button %s>Add</button>" % html_params(class_='add-kv btn btn-primary')))
        else:
            html.append(HTMLString("<button %s>Remove</button>" % html_params(class_='remove-kv btn btn-danger')))
        html.append("</div></div>")
        return ''.join(html)

    def __call__(self, field, **kwargs):
        style = '''
        <style>
        .json-key-input { text-align: right; }
        .row.kv-wrapper { margin-bottom: 0.5em; }
        </style>
        '''
        html = []
        html.append(HTMLString("<fieldset %s>" % html_params(class_=('form-group fs-kv-wrapper %s-wrapper' % field.id))))
        # html.append(HTMLString("<legend>Optional Properties</legend>"))
        o = json.loads(field._value())
        # new kv interface
        html.append(self._build_json_kv_comp(None, None))
        for k, v in iteritems(o):
            html.append(self._build_json_kv_comp(k, v))
        
        html.append("</fieldset>")
        row_kv_template = self._build_json_kv_comp('x', 'x')
        script = '''
        <script>
            (function(){
             
            function update_json(){
                // construct json
                var o = {};
                $('.%s-wrapper').find('.row').each(function(){
                    var k = $(this).find('.json-key-input').val();
                    var v = $(this).find('.json-value-input').val();
                    if(!k || !v) return;
                    var values = v.split(',');
                    var _values = [];
                    for(var i = 0; i < values.length; i++){
                        var _v = values[i].trim();
                        if(_v != '') _values.push(_v);
                    }
                    o[k.trim()] = _values;
                });

                
                $('#%s').val(JSON.stringify(o));
            }

            function add_key(k, v){
                var row = $('%s');
                row.appendTo($('.fs-kv-wrapper'));
                row.find('input.json-key-input').val(k);
                row.find('input.json-value-input').val(v);
                update_json();
                return false;
            }

            // wait for load jquery
            setTimeout(function(){
                $('.add-kv').click(function(){
                    var r = $(this).parents('.row');
                    var key = r.find('input.json-key-input');
                    var value = r.find('input.json-value-input');
                    if(key.val() && value.val()){
                        add_key(key.val(), value.val());
                        key.val('');
                        value.val('');
                    } else alert('Optional keyword or choices should be filled!');

                    return false; 
                });

                $('.remove-kv').on('click', function(){
                    $(this).parents('.row').remove();
                    update_json();
                    return false;
                });

                $(".json-value-input, .json-key-input").blur(function(){
                    update_json();
                });

            }, 100);

            })();
        </script>
        ''' % (field.id, field.id, row_kv_template,)
        ui = HTMLString(''.join(html))
        i = HTMLString("<input %s>" % html_params(id=field.id, name=field.id, type='hidden'))
        html = HTMLString("%s %s %s %s" % (style, i, ui, script,))
        return html


class JSONInput(Field):
    widget = JSONWidget()

    def _value(self):
        if self.data:
            return json.dumps(self.data)
        else:
            return u'{}'

    def process_formdata(self, valuelist):
        data = valuelist[0]
        if data:
            self.data = json.loads(data)
            for k, v in self.data.iteritems():
                self.data[k] = [e for e in v if e]
        else:
            data = {}


class PrototypeView(AdminModelView):
    form_excluded_columns = ['created_at', 'updated_at', 'products', 'updated_by', 'created_by']
    form_create_rules = ["name", "name_th", "properties", "check_properties", "optional_properties", "choose_properties"]
    column_list = ('name', 'name_th', 'properties', 'optional_properties',
                   'created_at', 'created_by',
                   'updated_at', 'updated_by', 'disabled')

    column_searchable_list = ('name', )
    column_sortable_list = ('name', )
    column_default_sort = 'name'

    def optional_prop_format(v, c, m, p):
        if m.optional_properties:
            return ["%s" % k for k, v in m.optional_properties.iteritems() ]
        else:
            return '-'

    column_formatters = {
            'optional_properties': optional_prop_format,
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    form_overrides = dict(optional_properties=JSONInput, 
                          choose_properties=JSONInput, 
                          check_properties=TagListField, 
                          properties=TagListField)
    form_args = dict(
        name_th=dict(validators=[DataRequired()])
    )

    form_widget_args = dict(name=dict(autocomplete='off'), name_th=dict(autocomplete='off'))

    def delete_model(self, model):
        model.disabled = True
        db.session.add(model)
        db.session.commit()
        return True
