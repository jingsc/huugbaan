# coding: utf-8
import os
import flask_admin as fa
import os.path as op
from flask_admin.contrib.sqla import ModelView
from jinja2 import Markup
from datetime import datetime
from werkzeug import secure_filename
from flask import url_for


# general view hide timestamp in the form
class HideTimestampView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at']


# generate hide timestamp view on runtime with class name
def build_excluded_column_view(cls_name, l=[]):
    config = { 
        'form_excluded_columns': (['created_at', 'updated_at']+l)
    }
    return type(cls_name, (ModelView,), config)


def display_timestamp_format(view, context, model, p):
    if getattr(model, p):
        return getattr(model,p).strftime('%Y-%m-%d %H:%I:%S')
    else:
        return '-'

# upload name generator : # prefix-datetime-file.format
def image_prefix_date_namegen(prefix='file'):
    def namegen(o, file_data):
        now = datetime.now().strftime('%Y%m%d%H%M%S%f') # year month day hour min sec microsec
        parts = list(op.splitext(file_data.filename))
        parts.insert(0, now)
        parts.insert(0, prefix)
        return secure_filename("%s-%s-%s%s" % tuple(parts))
    return namegen


# list image thumbnail
THUMB_SIZE = (100, 100,)
def list_thumbnail(attr, foldername, default_url=''):
    def _list_thumbnail(view, context, model, name):
        if not getattr(model, attr):
            return Markup('<img src="%s" width="%spx" height="%spx" />' % ((default_url,) + THUMB_SIZE))
        return Markup('<img src="%s" />' % url_for('static', 
                      filename=("%s/%s" % (foldername, fa.form.thumbgen_filename(getattr(model, attr))))))
    return _list_thumbnail
