# coding: utf-8
import os
import os.path as op
import datetime
import re
from copy import copy
from flask_admin import Admin, form, expose
from flask_admin.helpers import get_url
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.orm.attributes import flag_modified
from flask.blueprints import Blueprint
from jinja2 import Markup
from flask import url_for, request, redirect, flash
from ..models import db, Product, Prototype
from wtforms.widgets import  html_params, HTMLString
from .util import display_timestamp_format

try:
    from PIL import Image, ImageOps
except ImportError:
    Image = None
    ImageOps = None


# store images for products
folder_name = 'product_images'
file_path = op.join(op.dirname(__file__), ('../%s' % folder_name))
bp_product_view = Blueprint('product_view',  __name__,
                  static_url_path='/product_images', 
                  static_folder=file_path)


import json
class ProductView(ModelView):
    form_excluded_columns = ['slug', 'created_at', 'updated_at', 'scenes_products', 'vendors', 'created_by', 'updated_by']
    form_create_rules = ('name', 'description', 'product_code', 'prototype', 'image',)
    column_list = ('images', 'name', 'description', 
            'product_code', 'created_at', 'created_by',
            'updated_at', 'updated_by')
    column_searchable_list = ('name', 'product_code', 'properties')
    column_sortable_list = ('name', 'product_code', 'properties', 'created_at')
    column_default_sort = 'name'

    def properties_format(v, c, m, p):
        if m.properties:
            return ["%s => %s" % (k, v) for k, v in m.properties.iteritems() ]
        else:
            return 'NONE'


    def show_thumb(v, c, m, p):
        if m.images:
            for img in m.images:
                if m.images[img]["is_master"]:
                    return HTMLString("<img %s>" % html_params(src=m.images[img]["thumb_uri"]))
        return "NONE"


    column_formatters = {
            'properties': properties_format,
            'images': show_thumb,
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    form_extra_fields = {
        'image': form.ImageUploadField('Image',
               base_path=file_path,
               endpoint='product_view.static',
               thumbnail_size=(100, 100, True)),
        }


    def _extract_properties(self, params):
        properties = {}
        for k in params:
            match = re.search(r"properties\[(.*)\]", k)
            if match:
                attr = match.group(1)
                properties[attr] = params[k]

        return properties


    # config image processing
    thumb_size = ( 100, 100 )
    def _process_images(self, files, product):
        for fname in request.files:
            if fname == '': continue

            ( width, height ) = self.thumb_size
            fstorage = request.files[fname]
            # check format
            filename = fstorage.filename
            image = Image.open(fstorage)
            # print image.format
            # print filename
            if image.format in ('JPEG', 'PNG',):
                thumb = image.copy()
                if image.size[0] > width or image.size[1] > height:
                    thumb.thumbnail(self.thumb_size, Image.ANTIALIAS)
                    # resize thumb
                    ImageOps.fit(thumb, self.thumb_size, Image.ANTIALIAS)

                # save thumb
                now = datetime.datetime.now().strftime('%Y%m%d%H%I%S_')
                filename = now + filename
                with open(self._get_thumb_path(filename), 'wb') as fp:
                    thumb.save(fp, image.format)

                # save image
                with open(self._get_image_path(filename), 'wb') as fp:
                    image.save(fp, image.format)

                if not product.images:
                    product.images = {}
                    # first uploaded image set to be a master for show
                    is_master = True
                else: is_master = False

                # updated images field with structure
                product.images[filename] = {
                        'uri': get_url('product_view.static', filename=filename),
                        'thumb_uri': get_url('product_view.static', filename=self._get_thumb_filename(filename)),
                        'created_at': datetime.datetime.now().strftime("%Y-%m-%d %H:%I:%S"),
                        'is_master': is_master,
                    }
                # manually add mod
                flag_modified(product, 'images')
        return True


    def _remove_image(self, product, key):
        # delete the image from disk
        uri = self._get_image_path(key)
        if op.exists(uri):
            os.remove(uri)

        # delete the thumb from disk
        uri = self._get_thumb_path(key)
        if op.exists(uri):
            os.remove(uri)

        # delete from json
        del product.images[key]
        flag_modified(product, "images")


    def _get_image_path(self, filename):
        return op.join(file_path, filename)


    def _get_thumb_path(self, filename):
        thumb_filename = self._get_thumb_filename(filename)
        return op.join(file_path, thumb_filename)

    
    def _get_thumb_filename(self, filename):
        return ('%s-thumb%s' % op.splitext(filename))
