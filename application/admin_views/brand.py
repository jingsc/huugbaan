# coding: utf-8
import os
import os.path as op
import flask_admin as fa
from flask_admin.contrib.sqla import ModelView
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from .util import display_timestamp_format, image_prefix_date_namegen, list_thumbnail
from ..models import db


logo_path = op.join(op.dirname(__file__), '../static/brand_images')
try:
    os.mkdir(logo_path)
except OSError:
    pass

class BrandView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at', 'created_by', 'updated_by', 'products']
    column_list = ['image', 'name_en', 'name_th', 'presentation_en', 'presentation_th', 'disabled', 
                   'created_at', 'created_by', 'updated_at', 'updated_by']
    form_create_rules = ('name_en', 
                         'name_th', 
                         'presentation_en', 
                         'presentation_th', 
                         'image')
    form_edit_rules = form_create_rules + ('disabled',)
    column_searchable_list = ('name_en', )

    form_args = dict(
        name_en=dict(validators=[DataRequired()]),
        presentation_en=dict(validators=[DataRequired()])
    )

    column_formatters = {
        'created_at': display_timestamp_format,
        'updated_at': display_timestamp_format,
        'image': list_thumbnail('image', 'brand_images'),
    }

    form_extra_fields = dict(
            image=fa.form.ImageUploadField("Image",
                                            base_path=logo_path,
                                            namegen=image_prefix_date_namegen('brand-logo'),
                                            url_relative_path='brand_images/',
                                            thumbnail_size=(100, 100, True)))
    def delete_model(self, model):
        model.disabled = True
        db.session.add(model)
        db.session.commit()
        return True
