# coding: utf-8
import jinja2
import json
from flask_admin.contrib.sqla import ModelView
from flask_admin.model.form import InlineFormAdmin
from wtforms.fields import BooleanField, Field
from wtforms.widgets import CheckboxInput, HTMLString
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from .prototype import TagListField
from .util import display_timestamp_format
from ..models import Address
from .scene import GeolocationField


class BusinessHoursWidget(object):
    def __call__(self, field, **kwargs):
        days = ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"]
        template = """
        <div class='business-hours'>
        {% for day in days %}
        <div class='row business-hour-item'>
            <div style='margin-top:2em; width: 50px; float: left;'>
                <input type='checkbox' {% if field._value().get(day, None) %} checked {% endif %} /> {{ day }}
            </div>
            <div class='col-lg-5' style='text-align: center;'>
                <div class='col-lg-6'>
                    <label>Open</label>
                    <input class='form-control' 
                           {% if not field._value().get(day, None) %} readonly {% endif %}
                           type='text'
                           width='100%'
                           style='text-align: center;' 
                           value='{{ field._value().get(day, {}).get("open",'') }}'
                           name='{{ field.id }}-{{ loop.index0 }}-open' />
                </div>
                <div class='col-lg-6'>
                    <label>Close</label>
                    <input class='form-control' 
                           {% if not field._value().get(day, None) %} readonly {% endif %}
                           type='text' 
                           width='100%'
                           style='text-align: center;' 
                           value='{{ field._value().get(day, {}).get("close", '') }}'
                           name='{{ field.id }}-{{ loop.index0 }}-close' />
                </div>
                <input type='hidden' 
                       value='{{ field._get_json_string(day) }}' 
                       name="{{ field.id }}" />
            </div>
        </div>
        <br/>
        {% endfor %}
        </div>

        <script>
            (function(){
                var items = document.getElementsByClassName("business-hour-item");
                var inputTimeFormatMask = /^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]\s?([AaPp][mM])?$/;
                var inputTimeFormatErrorMsg = "Time format should be in HH:MM(AM or PM)"
                for(var i = 0; i < items.length; i++){
                   var item = items[i];
                   (function(){
                       var inputs = item.getElementsByTagName("input");
                       var chk = inputs[0];
                       var openText = inputs[1];
                       var closeText = inputs[2];
                       var input = inputs[3];

                       chk.onclick = function(){
                            if(!this.checked){
                               input.value = '{}';
                               openText.value = null;
                               closeText.value = null;

                               // reset uneditable
                               openText.readOnly = true;
                               closeText.readOnly = true;
                            } else {
                               var v = input.value;
                               if(v == '{}'){
                                    openText.value = "8:00";
                                    closeText.value = "17:00";
                               }
                               // enable editable
                               openText.readOnly = false;
                               closeText.readOnly = false;
                            }

                            updateBusinessHourValue();
                       }

                       // store old value for restoring after testing input format
                       var oldOpenValue = '';
                       openText.onfocus = function(){
                           oldOpenValue = this.value; 
                       }

                       // mask input
                       openText.onblur = function(){
                            var v = this.value 
                            // testing open format
                            if(! inputTimeFormatMask.test(v) && chk.checked){
                                this.value = oldOpenValue;
                                alert(inputTimeFormatErrorMsg);
                            }
                            updateBusinessHourValue();
                       }

                       var oldCloseValue = '';
                       closeText.onfocus = function(){
                           oldCloseValue = this.value; 
                       }

                       // mask input
                       closeText.onblur = function(){
                            var v = this.value 
                            if(! inputTimeFormatMask.test(v) && chk.checked){
                                this.value = oldCloseValue;
                                alert(inputTimeFormatErrorMsg);
                            }
                            updateBusinessHourValue();
                       }

                       var updateBusinessHourValue = function(){
                            var data = {}
                            if(openText.value) data["open"] = openText.value;
                            if(closeText.value) data["close"] = closeText.value;
                            input.value = JSON.stringify(data);
                       }
                   })();
                }
            })();
        </script>
        """
        return  HTMLString(jinja2.Template(template).render(field=field, days=days))


class BusinessHoursField(Field):
    widget = BusinessHoursWidget()
    def _value(self):
        return self.data or dict({})

    def _get_json_string(self, day):
        if self._value().get(day, None):
            return json.dumps(self.data[day])
        else:
            return u"{}"

    def process_formdata(self, valuelist):
        days = ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"]
        self.data = dict(zip(days, map(lambda r: {}, range(7))))
        if valuelist:
            _data = map(lambda d: json.loads(d or "{}"), valuelist)
            self.data = dict(zip(days, _data))
        else:
            self.data = dict(zip(days, map(lambda r: {}, range(7))))


class AddressView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at', 
                             'created_by', 'updated_by', 
                             'vendors', 'vendor_address', 'user_address']

    column_list = ['firstname', 'lastname', 'address1', 'address2', 'company', 'email', 'phone']

    column_searchable_list = ('firstname', 'lastname', 'company')

    form_args = dict(
        firstname=dict(validators=[DataRequired()]),
        lastname=dict(validators=[DataRequired()]),
        address1=dict(validators=[DataRequired()]),
        district=dict(validators=[DataRequired()]),
        city=dict(validators=[DataRequired()]),
        province=dict(validators=[DataRequired()]),
        zipcode=dict(validators=[DataRequired()]),
        phone=dict(validators=[DataRequired()]),
        email=dict(validators=[Email()]),
        business_hours=dict(label="Open Hour"),
    )

    column_formatters = {
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }


class AddressInlineForm(InlineFormAdmin):
    form_label = 'Address'
    form_excluded_columns = ['created_at', 'updated_at', 
                             'created_by', 'updated_by', 
                             'vendors', 'discriminator', 'parent_id', 
                             'parent_vendor', 'parent_user']

    form_args = dict(
        firstname=dict(validators=[DataRequired()]),
        lastname=dict(validators=[DataRequired()]),
        address1=dict(validators=[DataRequired()]),
        district=dict(validators=[DataRequired()]),
        city=dict(validators=[DataRequired()]),
        province=dict(validators=[DataRequired()]),
        zipcode=dict(validators=[DataRequired()]),
        phone=dict(validators=[DataRequired()]),
        email=dict(validators=[Email()]),
        in_used=dict(default=True)
    )

    form_overrides = dict(business_hours=BusinessHoursField, 
                          location=GeolocationField)

    form_widget_args = dict(is_main=dict(class_='chk-address'))

    # XXX NOT adding class thru render_kw is not work now
    # def postprocess_form(self, form):
    #     form.is_main = BooleanField("Main Address", render_kw={"class": 'chk-address'})
    #     return form

    def __init__(self):
        return super(AddressInlineForm, self).__init__(Address)
