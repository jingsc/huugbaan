# coding: utf-8
from flask_admin.contrib.sqla import ModelView
from wtforms.validators import DataRequired, NumberRange, ValidationError, Email
from .prototype import TagListField
from .util import display_timestamp_format


class VendorAddressView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at']
    column_list = ['vendor.name_en', 'address.firstname', 'address.phone',
                   'created_at', 'created_by', 'updated_at', 'updated_by']
    form_create_rules = ('vendor', 'address', 'is_main', 'in_used',)
    form_edit_rules = form_create_rules
    # column_searchable_list = ('name_en', 'brand_owners',)

    form_args = dict(
        vendor=dict(validators=[DataRequired()]),
        address=dict(validators=[DataRequired()])
    )

    column_formatters = {
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }
