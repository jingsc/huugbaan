# coding: utf-8
import os
import os.path as op
import datetime
import re
import json
import wtforms.widgets as wg
import wtforms.fields as fd
import wtforms.validators as vt
from copy import copy
from flask_wtf import Form
from flask_admin import Admin, form, expose
from flask_admin.helpers import get_url
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.orm.attributes import flag_modified
from flask.blueprints import Blueprint
from jinja2 import Markup
from flask import url_for, request, redirect, flash
from ..models import db, Product, Prototype, Brand, Style
from wtforms.widgets import  html_params, HTMLString
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import ValidationError
from .util import display_timestamp_format

try:
    from PIL import Image, ImageOps
except ImportError:
    Image = None
    ImageOps = None


# store images for products
folder_name = 'product_images'
file_path = op.join(op.dirname(__file__), ('../%s' % folder_name))
bp_product_view = Blueprint('product_view',  __name__,
                  static_url_path='/product_images', 
                  static_folder=file_path)


# custom form
class ProductForm(Form):
    name_en = fd.StringField('Name(EN)',validators=[vt.DataRequired(), vt.Length(max=250)])
    description_en = fd.HiddenField('Description(EN)', validators=[vt.DataRequired()])
    name_th = fd.StringField('Name(TH)',validators=[vt.DataRequired(), vt.Length(max=250)])
    description_th = fd.HiddenField('Description(TH)', validators=[vt.DataRequired()])
    product_code = fd.StringField('Product Code',validators=[vt.DataRequired(), vt.Length(max=150)])
    brand = QuerySelectField('Brand', allow_blank=True,
                              query_factory=lambda : Brand.query.filter_by(disabled=False).all(),
                              get_pk=lambda r: r.id,
                              get_label=lambda r: r.name_en)
    prototype = QuerySelectField('Prototype', allow_blank=False,
                              query_factory=lambda : Prototype.query.filter_by(disabled=False).all(),
                              get_pk=lambda r: r.id,
                              get_label=lambda r: r.name)
    styles = QuerySelectMultipleField('Styles', allow_blank=False,
                              query_factory=lambda : Style.query.filter_by(disabled=False).all(),
                              get_pk=lambda r: r.id,
                              get_label=lambda r: r.name)
    
    def validate_name_en(form, field):
        product = Product.query.filter_by(name_en=field.data).first()
        if product:
            raise ValidationError("Should be uniqness.")

    def validate_name_th(form, field):
        product = Product.query.filter_by(name_th=field.data).first()
        if product:
            raise ValidationError("Should be uniqness.")

    def validate_product_code(form, field):
        product = Product.query.filter_by(product_code=field.data).first()
        if product:
            raise ValidationError("Should be uniqness.")


class ProductUpdateForm(ProductForm):
    id = fd.HiddenField()

    prototype = QuerySelectField('Prototype', allow_blank=True,
                              query_factory=lambda : Prototype.query.all(),
                              get_pk=lambda r: r.id,
                              get_label=lambda r: r.name)

    def validate_name_en(form, field):
        id = form.data.get('id')
        product = Product.query.filter_by(name_en=field.data).filter(Product.id != id).first()
        if product:
            raise ValidationError("Should be uniqness.")

    def validate_name_th(form, field):
        id = form.data.get('id')
        product = Product.query.filter_by(name_th=field.data).filter(Product.id != id).first()
        if product:
            raise ValidationError("Should be uniqness.")

    def validate_product_code(form, field):
        id = form.data.get('id')
        product = Product.query.filter_by(product_code=field.data).filter(Product.id != id).first()
        if product:
            raise ValidationError("Should be uniqness.")


class ProductView(ModelView):
    form_excluded_columns = ['slug', 'created_at', 'updated_at', 'scenes_products', 'vendors']
    # form_create_rules = ('name', 'description', 'slug', 'img_uri_1', 'img_uri_2', 'img_uri_3', 'img_uri_4', 'img_uri_5', 'img_uri_6')
    # form_edit_rules = ('name', 'description', 'slug', 'img_uri_1', 'img_uri_2', 'img_uri_3', 'img_uri_4', 'img_uri_5', 'img_uri_6')
    column_list = ('images', 'name_en', 'prototype.name', 
            'product_code', 'brand', 'created_at', 'created_by',
            'updated_at', 'updated_by')
    column_searchable_list = ('name_en', 'product_code', 'properties')
    column_sortable_list = ('name_en', 'product_code', 'properties', 'created_at')
    column_defualt_sort = 'name_en'
    column_labels = {
        'prototype.name': "Prototype"
    }

    MIN_IMAGE_SIZE = ( 500, 166 )

    def properties_format(v, c, m, p):
        if m.properties:
            return ["%s => %s" % (k, v) for k, v in m.properties.iteritems() ]
        else:
            return 'NONE'


    def show_thumb(v, c, m, p):
        if m.images:
            for img in m.images:
                if m.images[img]["is_master"]:
                    return HTMLString("<img %s>" % html_params(src=m.images[img]["thumb_uri"]))
        return "NONE"


    column_formatters = {
            'properties': properties_format,
            'images': show_thumb,
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    # form_extra_fields = {
    #     'image': form.ImageUploadField('Image',
    #            base_path=file_path,
    #            endpoint='product_view.static',
    #            thumbnail_size=(100, 100, True)),
    #     }


    def _extract_properties(self, params, default_properties={}):
        properties = default_properties
        for k in params:
            match = re.search(r"^properties\[(.*)\]", k)
            if match:
                attr = match.group(1)
                properties[attr] = params[k]

            check_match = re.search(r"^check_properties\[(.*)\]", k)
            if check_match:
                attr = check_match.group(1)
                properties[attr] = bool(params[k])

        return properties
    

    @expose('/new/', methods=('GET', 'POST'))
    def create_view(self):
        form = ProductForm()
        images = request.files.getlist('images[]')
        has_uploaded_image = False
        try:
            has_uploaded_image = (images[0].content_length != 0)
            if not has_uploaded_image:
                flash("Please upload a product image.", 'error')
        except Exception, e:
            pass

        if request.method == 'POST' and \
                form.validate_on_submit() and \
                has_uploaded_image: 

            p = Product()
            form.populate_obj(p)

            # create properties
            if p.prototype:
                properties = p.prototype.create_default_props()
            else:
                properties = {}
            p.properties = self._extract_properties(request.form, default_properties=properties)

            for img_file in images:
                if img_file.filename != '':
                    set_master = ( img_file.filename == request.form.get("master_filename") )
                    self._save_image(img_file, p, set_master)

            db.session.add(p)
            db.session.commit()
            flash('Create product complete!')
            return redirect(self.get_url('.index_view'))
        else: 
            prototypes = [ p.attrs() for p in Prototype.query.filter_by(disabled=False).all() ]
            prototypes = sorted(prototypes, key=lambda r: r['name'])
            ref_brands = Brand.query.all()
            return self.render("admin/product_create.html", 
                                return_url=self.get_url('.index_view'),
                                brands=ref_brands,
                                form=form,
                                min_image_size=self.MIN_IMAGE_SIZE,
                                prototypes=prototypes)
    

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        _id = request.args['id']
        if _id: product = Product.query.get(_id)
        else: abort(400)

        form = ProductUpdateForm(obj=product)

        if request.method == 'POST' and form.validate_on_submit():
            form.populate_obj(product)

            # print request.form.getlist('remove_images')
            for img in request.form.getlist('remove_images'):
                self._remove_image(product, img)

            master_img = request.form.get('master_image')
            if master_img:
                self._set_master_image(product, master_img)

            # blank upload <FileStorage: u'' ('application/octet-stream')>
            images = request.files.getlist("images[]")
            for img_file in images:
                if img_file.filename != '':
                    self._save_image(img_file, product, img_file.filename == master_img)

            # set master
            
            if product.prototype:
                default_props = product.prototype.create_default_props()
            else:
                default_props = {}
            properties = self._extract_properties(request.form, default_properties=default_props)
            product.properties = properties
            db.session.add(product)
            try:
                db.session.commit()
            except Exception, e:
                flash(e.message, 'error')
                return redirect(self.get_url('.edit_view'))

            flash('Product has been updated!')
            submit = request.form.get('submit')
            if submit == 'Save and continue':
                return redirect(self.get_url('.edit_view', id=product.id))
            return redirect(self.get_url('.index_view'))


        # not post or form not valid execute here
        def _product_images_format(key):
            product.images[key]['name'] = key 
            return product.images[key]

        if product.images:
            images = sorted(map(_product_images_format, product.images), 
                            key=lambda r: r['created_at'], reverse=False)
        else:
            images = []

        ref_brands = Brand.query.all()

        return self.render("admin/product_edit.html", 
                            product=product,
                            images=images,
                            brands=ref_brands,
                            form=form,
                            min_image_size=self.MIN_IMAGE_SIZE,
                            return_url=self.get_url('.index_view'))


    def _validate_product(self, p, on_create=True):
        msg = []
        if not p.name_en:
            msg.append('Product name en should not be blank')

        if Product.query.filter_by(name_en=p.name_en).first():
            msg.append('Product name en should not duplicated')

        if not p.name_th:
            msg.append('Product name th should not be blank')

        if Product.query.filter_by(name_th=p.name_th).first():
            msg.append('Product name th should not duplicated')

        if not p.prototype_id:
            msg.append('Please choose a prototype for this product')

        if on_create:
            if p.product_code:
                should_not_found_product = Product.query.filter(db.not_(Product.id==p.id)).\
                                                         filter_by(product_code=p.product_code).first()
                if should_not_found_product:
                    msg.append('The product code is already exists.')
        else:
            if p.product_code:
                should_not_found_product = Product.query.filter(db.not_(Product.id==p.id)).filter_by(product_code=p.product_code).first()
                if should_not_found_product:
                    flash('The product code is already exists.', 'error')
                    return redirect(self.get_url('.index_view'))
        return msg


    # config image processing
    thumb_size = ( 100, 100 )
    mid_size = ( 300, 300 )
    def _save_image(self, img_storage, product, is_master=False):
        ( width, height ) = self.thumb_size
        ( m_width, m_height ) = self.mid_size
        fstorage = img_storage
        # check format
        filename = fstorage.filename
        image = Image.open(fstorage)
        if image.format in ('JPEG', 'PNG',):
            thumb = image.copy()
            mid = image.copy()

            # make new filename with seed
            now = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f_') # year month day hour min sec microsec
            filename = now + filename

            if image.size[0] > width or image.size[1] > height:
                thumb.thumbnail(self.thumb_size, Image.ANTIALIAS)
                # resize thumb
                ImageOps.fit(thumb, self.thumb_size, Image.ANTIALIAS)

            with open(self._get_thumb_path(filename), 'wb') as fp:
                thumb.save(fp, image.format)

            if image.size[0] > m_width or image.size[1] > m_height:
                mid.thumbnail(self.mid_size, Image.ANTIALIAS)
                # resize mid thumb size
                ImageOps.fit(mid, self.mid_size, Image.ANTIALIAS)

            # save mid
            with open(self._get_mid_thumb_path(filename), 'wb') as fp:
                mid.save(fp, image.format)

            # save image
            with open(self._get_image_path(filename), 'wb') as fp:
                image.save(fp, image.format)

            # try to search master if not be set
            if not product.images:
                product.images = {}
            # if not is_master:
            #     if not product.images:
            #         product.images = {}
            #         # first uploaded image set to be a master for show
            #         is_master = True
            #     else: is_master = False

            # XXX product images structure
            # updated images field with structure
            product.images[filename] = {
                    'uri': get_url('product_view.static', filename=filename),
                    'thumb_uri': get_url('product_view.static', filename=self._get_thumb_filename(filename)),
                    'mid_thumb_uri': get_url('product_view.static', filename=self._get_mid_thumb_filename(filename)),
                    'created_at': datetime.datetime.now().strftime("%Y-%m-%d %H:%I:%S"),
                    'is_master': is_master,
                }
            # manually add mod
            flag_modified(product, 'images')
        return True


    def _set_master_image(self, product, filename):
        for img_name in product.images:
            product.images[img_name]["is_master"] = False
            if img_name == filename:
                product.images[img_name]["is_master"] = True
        flag_modified(product, "images")


    def _remove_image(self, product, filename):
        # delete the image from disk
        uri = self._get_image_path(filename)
        if op.exists(uri):
            os.remove(uri)

        # delete the thumb from disk
        uri = self._get_thumb_path(filename)
        if op.exists(uri):
            os.remove(uri)

        # delete the mid thumb from disk
        uri = self._get_mid_thumb_path(filename)
        if op.exists(uri):
            os.remove(uri)

        # replace master
        replace_master = product.images[filename]['is_master']
        
        # delete from json
        del product.images[filename]

        if replace_master:
            new_master = product.images.keys() and product.images.keys()[0]
            if new_master:
                product.images[new_master]['is_master'] = True
        flag_modified(product, "images")


    def _get_image_path(self, filename):
        return op.join(file_path, filename)


    def _get_thumb_path(self, filename):
        thumb_filename = self._get_thumb_filename(filename)
        return op.join(file_path, thumb_filename)


    def _get_mid_thumb_path(self, filename):
        thumb_filename = self._get_mid_thumb_filename(filename)
        return op.join(file_path, thumb_filename)

    
    def _get_thumb_filename(self, filename):
        return ('%s-thumb%s' % op.splitext(filename))


    def _get_mid_thumb_filename(self, filename):
        return ('%s-mid-thumb%s' % op.splitext(filename))
