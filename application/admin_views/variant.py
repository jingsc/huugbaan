# coding: utf-8
from flask import flash, request
from flask_admin.contrib.sqla import ModelView
from ..models import Variant, db
from wtforms.validators import DataRequired, NumberRange, ValidationError
from .util import display_timestamp_format
from wtforms import Field, SelectField, TextField, TextAreaField
from wtforms.widgets import TextInput, html_params, HTMLString, HiddenInput, TextArea
from flask_admin.form import rules


class VariantView(ModelView):
    form_excluded_columns = ['created_at', 'updated_at', 'deleted_at']
    column_list = ['vendor.name_en', 'product.name_en', 'sku', 'price', 'is_master', 'created_at', 'created_by', 'updated_at', 'updated_by']
    form_create_rules = ['product', 'vendor', 'sku', 'price', 'reference_url', 'is_master']
    form_edit_rules = form_create_rules
    column_labels = {
            'vendor.name_en': 'Vendor Name',
            'product.name_en': 'Product Name',
            }

    column_formatters = {
            'created_at': display_timestamp_format,
            'updated_at': display_timestamp_format,
            }

    def uniqness_variant(form, field):
        pid = form.product.data.id
        vid = form.vendor.data.id
        sku = form.sku.data
        if request.endpoint == 'variant.create_view':
            if pid and vid:
                variant = Variant.query.filter_by(product_id=int(pid), vendor_id=int(vid)).first()
        else: # update
            id = request.args.get('id')
            # exclude self id
            variant = Variant.query.\
                      filter_by(product_id=int(pid), vendor_id=int(vid)).\
                      filter(Variant.id != id).first()

        if variant:
            raise(ValidationError("This variant is existing"))


    def uniqness_sku(form, field):
        pid = form.product.data.id
        sku = form.sku.data
        if pid:
            variants_by_product = Variant.query.filter(Variant.product_id != int(pid)).all()
            skus = [v.sku for v in variants_by_product]
            if sku in skus:
                raise(ValidationError("This sku should not be duplicated"))


    form_args = dict(
        product=dict(validators=[DataRequired(), uniqness_variant]),
        vendor=dict(validators=[DataRequired(), uniqness_variant]),
        sku=dict(validators=[DataRequired(), uniqness_sku]),
        price=dict(validators=[DataRequired(), NumberRange(min=0, message="Price should be more than 0.")]),
    )
