# coding: utf-8
from flask_admin.contrib.sqla import ModelView
from flask_login import login_required, logout_user, \
                        current_user, login_user
import flask_admin
from flask import redirect, url_for, flash

class AdminModelView(ModelView):
    def is_accessible(self):
        if current_user.is_authenticated and current_user.is_admin:
            return True
        return False


class MyAdminIndexView(flask_admin.AdminIndexView):
    @flask_admin.expose('/')
    def index(self):
        if not current_user.is_authenticated or \
           not current_user.is_admin:
            flash('You dont have permission to access this page.')
            return redirect(url_for('site.login'))
        return super(MyAdminIndexView, self).index()
