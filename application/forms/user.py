# coding: utf-8
from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email, EqualTo
from ..models import User


class UserProfile(Form):
    # name = StringField('Username',
    #                    validators=[DataRequired("Username shouldn't be empty.")])

    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])

    # email = StringField('Email',
    #                     validators=[
    #                         DataRequired(message="Email shouldn't be empty."),
    #                         Email(message='Email format is not correct.')
    #                     ])

    def validate_name(self, field):
        user = User.query.filter(User.name == self.name.data).first()
        if user:
            raise ValueError('This username already exists.')

    def validate_email(self, field):
        user = User.query.filter(User.email == self.email.data).first()
        if user:
            raise ValueError('This email already exists.')
