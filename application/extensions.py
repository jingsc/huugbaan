#from flask.ext.cache import Cache
# Setup flask cache
#cache = Cache()

# CustomJSONEncoder ------------------------
from flask.json import JSONEncoder
import calendar
from datetime import datetime
from datetime import date
from decimal import Decimal

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                if obj.utcoffset() is not None:
                    obj = obj - obj.utcoffset()
                d, t = obj.isoformat().split('T')
                t = t.split('.')[0]
                return "%s %s" % (d, t)
            if isinstance(obj, date):
                return str(obj.isoformat())
            if isinstance(obj, Decimal):
                return str(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)

        return JSONEncoder.default(self, obj)
