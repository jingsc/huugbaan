# coding: utf-8
import sys
import os

# Insert project root path to sys.path
project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if project_path not in sys.path:
    sys.path.insert(0, project_path)

import time
import logging
from flask import Flask, request, url_for, g, render_template
from flask_wtf.csrf import CsrfProtect
from flask_debugtoolbar import DebugToolbarExtension
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.contrib.fixers import ProxyFix
from six import iteritems
# from .utils.account import get_current_user
from config import load_config

# convert python's encoding to utf8
try:
    from imp import reload

    reload(sys)
    sys.setdefaultencoding('utf8')
except (AttributeError, NameError):
    pass


def create_app():
    """Create Flask app."""
    config = load_config()

    app = Flask(__name__)
    app.config.from_object(config)

    if not hasattr(app, 'production'):
        app.production = not app.debug and not app.testing

    # Proxy fix
    app.wsgi_app = ProxyFix(app.wsgi_app)

    # CSRF protect
    CsrfProtect(app)

    if app.debug or app.testing:
        DebugToolbarExtension(app)

        # Serve static files
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/pages': os.path.join(app.config.get('PROJECT_PATH'), 'application/pages')
        })
    else:
        # Log errors to stderr in production mode
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.ERROR)

        # Enable Sentry
        if app.config.get('SENTRY_DSN'):
            from .utils.sentry import sentry

            sentry.init_app(app, dsn=app.config.get('SENTRY_DSN'))

        # Serve static files
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static': os.path.join(app.config.get('PROJECT_PATH'), 'output/static'),
            '/pkg': os.path.join(app.config.get('PROJECT_PATH'), 'output/pkg'),
            '/pages': os.path.join(app.config.get('PROJECT_PATH'), 'output/pages')
        })

    # Register components
    register_json_encoder(app)
    register_db(app)
    register_routes(app)
    register_jinja(app)
    register_error_handle(app)
    register_flask_login(app)
    # register_hooks(app)
    register_biz_hooks(app)

    import warnings
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", "Fields missing from ruleset", UserWarning)
        register_admin(app)

    return app


def register_jinja(app):
    """Register jinja filters, vars, functions."""
    import jinja2
    from .utils import filters, permissions, helpers

    if app.debug or app.testing:
        my_loader = jinja2.ChoiceLoader([
            app.jinja_loader,
            jinja2.FileSystemLoader([
                os.path.join(app.config.get('PROJECT_PATH'), 'application/macros'),
                os.path.join(app.config.get('PROJECT_PATH'), 'application/pages')
            ])
        ])
    else:
        my_loader = jinja2.ChoiceLoader([
            app.jinja_loader,
            jinja2.FileSystemLoader([
                os.path.join(app.config.get('PROJECT_PATH'), 'application/macros'),
                os.path.join(app.config.get('PROJECT_PATH'), 'application/pages')
            ])
        ])
        # my_loader = jinja2.ChoiceLoader([
        #     app.jinja_loader,
        #     jinja2.FileSystemLoader([
        #         os.path.join(app.config.get('PROJECT_PATH'), 'output/macros'),
        #         os.path.join(app.config.get('PROJECT_PATH'), 'output/pages')
        #     ])
        # ])
    app.jinja_loader = my_loader

    app.jinja_env.filters.update({
        'timesince': filters.timesince,
        'baht': filters.baht
    })

    app.jinja_env.add_extension('jinja2.ext.loopcontrols')

    def url_for_other_page(page):
        """Generate url for pagination."""
        view_args = request.view_args.copy()
        args = request.args.copy().to_dict()
        combined_args = dict(view_args.items() + args.items())
        combined_args['page'] = page
        return url_for(request.endpoint, **combined_args)

    rules = {}
    for endpoint, _rules in iteritems(app.url_map._rules_by_endpoint):
        if any(item in endpoint for item in ['_debug_toolbar', 'debugtoolbar', 'static']):
            continue
        rules[endpoint] = [{'rule': rule.rule} for rule in _rules]

    app.jinja_env.globals.update({
        'absolute_url_for': helpers.absolute_url_for,
        'url_for_other_page': url_for_other_page,
        'rules': rules,
        'permissions': permissions
    })

    from social.apps.flask_app.template_filters import backends
    app.context_processor(backends)


def register_db(app):
    """Register models."""
    from .models import db
    db.init_app(app)

    from social.storage.sqlalchemy_orm import SQLAlchemyMixin
    def _commit(cls):
        cls._session().commit();
    
    # override class method _flush to always use commit
    setattr(SQLAlchemyMixin, '_flush', classmethod(_commit))

    from social.apps.flask_app.default.models import init_social
    init_social(app, db.session)


def register_routes(app):
    """Register routes."""
    from social.apps.flask_app.routes import social_auth
    app.register_blueprint(social_auth)

    from . import controllers
    from flask.blueprints import Blueprint

    for module in _import_submodules_from_package(controllers):
        bp = getattr(module, 'bp')
        if bp and isinstance(bp, Blueprint):
            app.register_blueprint(bp)


def register_error_handle(app):
    """Register HTTP error pages."""

    @app.errorhandler(403)
    def page_403(error):
        return render_template('site/403/403.html'), 403

    @app.errorhandler(404)
    def page_404(error):
        return render_template('site/404/404.html'), 404

    @app.errorhandler(500)
    def page_500(error):
        return render_template('site/500/500.html'), 500


import flask_login
def register_flask_login(app):
    from application.models import User

    login_manager = flask_login.LoginManager()
    login_manager.login_view = 'site.login'
    # login_manager.login_message = ''
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(userid):
        try:
            u = User.query.get(int(userid))
            if u.is_active:
               return u
            else:
               return None
        except (TypeError, ValueError):
            pass

    @app.before_request
    def global_user():
        g.user = flask_login.current_user

    # Make current user available on templates
    @app.context_processor
    def inject_user():
        try:
            return {'user': g.user}
        except AttributeError:
            return {'user': None}


def register_hooks(app):
    """Register hooks."""

    @app.before_request
    def before_request():
        g.user = get_current_user()
        if g.user and g.user.is_admin:
            g._before_request_time = time.time()

    @app.after_request
    def after_request(response):
        if hasattr(g, '_before_request_time'):
            delta = time.time() - g._before_request_time
            response.headers['X-Render-Time'] = delta * 1000
        return response


def register_biz_hooks(app):
    # cart status and items
    @app.context_processor
    def inject_item_count_in_cart():
        try:
            if flask_login.current_user.is_authenticated:
                item_count = flask_login.current_user.get_item_count_in_cart()
                cart_order = flask_login.current_user.cart_order
                return {'item_count_in_cart': sum([li.qty for li in cart_order.line_items]),
                        'line_items': cart_order.line_items }
            return {'item_count_in_cart': 0,
                    'line_items': [] }
        except AttributeError:
            return {'item_count_in_cart': 0,
                    'line_items': [] }


def register_admin(app):
    """Register Admin page"""
    from flask_admin import Admin
    from flask_admin.contrib.sqla import ModelView
    from .admin_views._base import MyAdminIndexView
    admin = Admin(app, name='Backend', template_mode='bootstrap3', index_view=MyAdminIndexView())

    from .models import ( 
            User, Product, Vendor, 
            db, Scene, Variant, 
            Prototype, SceneType,
            Brand, Address, Ord, Style )

    from .admin_views import HideTimestampView, build_excluded_column_view as excv
    from .admin_views import UserView
    admin.add_view(UserView(User, db.session))

    from .admin_views import StyleView
    admin.add_view(StyleView(Style, db.session))

    # admin.add_view(ModelView(Property, db.session))
    from .admin_views import PrototypeView
    admin.add_view(PrototypeView(Prototype, db.session))

    from .admin_views import BrandView
    admin.add_view(BrandView(Brand, db.session))

    # custom admin prouct
    from .admin_views import ProductView, bp_product_view
    admin.add_view(ProductView(Product, db.session))
    # admin.add_view(ModelView(Product, db.session))
    app.register_blueprint(bp_product_view) # for product static images

    from .admin_views import VendorView
    admin.add_view(VendorView(Vendor, db.session))

    from .admin_views import VariantView
    admin.add_view(VariantView(Variant, db.session))

    # admin.add_view(excv('SceceTypeView', ['scenes'])(SceneType, db.session))
    from .admin_views import SceneTypeView
    admin.add_view(SceneTypeView(SceneType, db.session))

    # custom admin scene
    from .admin_views import SceneView, bp_scene_view
    admin.add_view(SceneView(Scene, db.session))
    app.register_blueprint(bp_scene_view) # for scene static images

    # Normal View
    # from .admin_views import AddressView
    # admin.add_view(AddressView(Address, db.session))

    # from .admin_views import VendorAddressView
    # admin.add_view(VendorAddressView(VendorAddress, db.session))

    admin.add_view(ModelView(Ord, db.session))


def register_json_encoder(app):
    from flask.json import JSONEncoder
    # import calendar
    from datetime import datetime
    from datetime import date
    from decimal import Decimal

    class CustomJSONEncoder(JSONEncoder):
        def default(self, obj):
            try:
                if isinstance(obj, datetime):
                    if obj.utcoffset() is not None:
                        obj = obj - obj.utcoffset()
                    d, t = obj.isoformat().split('T')
                    t = t.split('.')[0]
                    return "%s %s" % (d, t)
                if isinstance(obj, date):
                    return str(obj.isoformat())
                if isinstance(obj, Decimal):
                    return str(obj)
                iterable = iter(obj)
            except TypeError:
                pass
            else:
                return list(iterable)

            return JSONEncoder.default(self, obj)


    app.json_encoder = CustomJSONEncoder


def _get_template_name(template_reference):
    """Get current template name."""
    return template_reference._TemplateReference__context.name


def _import_submodules_from_package(package):
    import pkgutil

    modules = []
    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__,
                                                         prefix=package.__name__ + "."):
        modules.append(__import__(modname, fromlist="dummy"))
    return modules
